open System
open System.IO
open System.Net.Sockets
open FSharp.Data

type Json = JsonProvider<"./sample.json", SampleIsList=true, RootName="message">

let send (msg : Json.Message) (writer : StreamWriter) (logger: StreamWriter) =
    let line = msg.JsonValue.ToString(JsonSaveOptions.DisableFormatting)
    logger.WriteLine(line)
    logger.Flush()
    writer.WriteLine(line)
    writer.Flush()

let join name key color =
    Json.Message(msgType = "join", data = Json.DecimalOrData(Json.Data(name = Some(name), key = Some(key), color = Some(color))))

let throttle (value : Decimal) =
    Json.Message(msgType = "throttle", data = Json.DecimalOrData(value))

let ping = Json.Message(msgType = "ping", data = null)

[<EntryPoint>]
let main args =
    let (host, portString, botName, botKey) = 
      args |> (function [|a;b;c;d|] -> a, b, c, d | _ -> failwith "Invalid param array")
    let port = Convert.ToInt32(portString)

    printfn "Connecting to %s:%d as %s/%s" host port botName botKey

    use client = new TcpClient(host, port)
    let stream = client.GetStream()
    let reader = new StreamReader(stream)
    let writer = new StreamWriter(stream)
    let logger = new StreamWriter("log.txt")

    send (join botName botKey "blue") writer logger

    let processNextLine nextLine =
        match nextLine with
        | null -> false
        | line ->
            try 
                let msg = Json.Parse(line)
                logger.WriteLine(msg.JsonValue.ToString())
                logger.Flush()
                match msg.MsgType with
                | "carPositions" -> send (throttle 0.6m) writer logger
                | "join" -> printfn "Joined"; send (ping) writer logger
                | "gameInit" -> printfn "Race init"; send (ping) writer logger
                | "gameEnd" -> printfn "Race ends"; send (ping) writer logger
                | "gameStart" -> printfn "Race starts"; send (ping) writer logger
                | _ -> send (ping) writer logger
                true
            with
            | ex -> true

    while processNextLine (reader.ReadLine()) do 
        ()

    0