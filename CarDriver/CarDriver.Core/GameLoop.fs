﻿module GameLoop

open System
open System.Collections.Generic
open System.IO
open System.Net.Sockets
open System.Threading
open System.Threading.Tasks
open Messaging

type Game (host,port,updateHandler, log:int -> string -> unit, aiHandler) = 

    let sendMsg (writer:StreamWriter) (msg:string) =
        writer.WriteLine(msg)
        writer.Flush()

    let client = new TcpClient(host, port)
    let stream = client.GetStream()
    let reader = new StreamReader(stream)
    let writer = new StreamWriter(stream)

    let send = sendMsg writer

    let logGenericMessages log message =

        let timeToString time ticks =
            let time = TimeSpan.FromMilliseconds(float time).ToString(@"m\:ss\.fff")
            sprintf "%ss (%dgt)" time ticks
        let carToString c = sprintf "%s (%s)" c.Name c.Colour

        match message with
        | Join -> log <| sprintf "Joined server %s:%d" host port 
        | YourCar c -> c |> carToString |> sprintf "Our Car is %s" |> log
        | Race r -> 
            let msg = match r.Session with
                      | Qualifying x -> sprintf " Type=Qualifying"
                      | FullRace r -> sprintf " Type=FullRace Laps=%d" r.LapCount
                      | QuickRace r -> sprintf " Type=QuickRace Laps=%d" r.LapCount

            sprintf "Session started Track=%s%s" r.Track.Name msg |> log
             
        | GameStart -> log "Race started"
        | CarPositions cps -> ()
        | LapFinished lap -> 
            let lapTime = timeToString lap.Time.LapMs lap.Time.LapTicks
            let raceTime = timeToString lap.RaceTime.RaceMs lap.RaceTime.RaceTicks
            let msg = sprintf "Lap %d %s Lap=%s Race=%s" lap.Time.Lap (carToString lap.CarId) lapTime raceTime  
            log msg
        | TurboAvailable t -> ()
        | TurboStart c -> () 
        | TurboEnd c -> () 
        | Crash c -> c |> carToString |> sprintf "CRASHED: %s" |> log
        | Spawn c -> c |> carToString |> sprintf "Restored: %s" |> log
        | Dnf (c,r)  -> log <| sprintf "Disqualified: %s Reason=%s" (carToString c) r
        | Finish c -> c |> carToString |> sprintf "Finished: %s" |> log
        | GameEnd results -> 
            let toMsg i (result:RaceResult) = 
                let bestLap = Option.fold (fun _ lap -> timeToString lap.LapMs lap.LapTicks) "(None)" result.BestLap
                let raceTime = Option.fold (fun _ raceTime -> timeToString raceTime.RaceMs raceTime.RaceTicks) "(DNF)" result.RaceTime     
                
                String.Format("{0} {1,-30} Time={2} Lap={3}", (i+1), (carToString result.CarId), raceTime, bestLap)

            log "Race finished - Results:"
            results |> Seq.mapi toMsg |> Seq.iter log
        | TournamentEnd -> log "Tournament finished"
        | Unknown x -> log x
    
    let handleYourCar gameState = 
         match gameState.MessageReceived with 
         | YourCar carId -> { gameState with YourCar=carId } 
         | _ -> gameState

    let handleRaceData gameState =

        let initialiseObservedVels race = 
            race.Cars |> Map.map (fun id _ -> Map.empty)

        let laneOptions lane = 
            let laneId = lane.Lane.Index
            match lane.LeftLane,lane.RightLane with 
            | None,None -> [ (laneId, laneId) ]
            | Some left, None -> [ (laneId,left); (laneId,laneId) ]
            | None, Some right -> [ (laneId,laneId); (laneId,right) ]
            | Some left, Some right -> [ (laneId,left); (laneId,right) ]

        let seedTargetVels race = 
            let getTargetVelocity (key:TargetVKey) = 
                let piece = race.Track.Pieces.Item(key.PId)
                let lane = race.Track.Lanes.[key.EndLn]
                let laneOffset = lane.DistFromCentre
                
                match piece.Spec with 
                | Bend b -> 
                    let trueR = if b.Angle > 0. then b.Radius - laneOffset else b.Radius + laneOffset
                    System.Math.Sqrt(trueR) * 0.65
                | _ -> 30.0

            let targetKeys = 
                race.Track.Pieces
                |> Seq.collect (fun tp -> tp.Lanes |> Array.map (fun lane -> tp.Id,lane))
                |> Seq.map (fun (id, lane) -> id, laneOptions lane)
                |> Seq.collect (fun (id,lanes) -> lanes |> List.map (fun (lStart, lEnd) -> { PId=id; StartLn=lStart; EndLn=lEnd; } ))
                                
            targetKeys 
            |> Seq.map (fun k -> k, getTargetVelocity k) 
            |> Seq.map (fun (k,v) -> k, { Velocity = v } )

        match gameState.MessageReceived with 
        | Race r -> 
            let targetVels = new Dictionary<_,_>();
            seedTargetVels r |> Seq.iter (fun (k,v) -> targetVels.Add(k,v))

            { gameState with Race = r; TargetVels = targetVels; TargetVelKeys =  targetVels.Keys |> Seq.toList } 
        | _ -> gameState


    let handleCrash gameState = 

        match gameState.MessageReceived with 
        | Crash carId when carId = gameState.YourCar -> 
            let angVel = gameState.Telemetry.AngVel
            
            let degreeDelta = 2.0 * System.Math.PI / 180.0
            let oldMaxAngle = gameState.Constants.MaxAngle
            let maxAngle = if (abs gameState.CarPos.Angle) < oldMaxAngle - degreeDelta then (abs gameState.CarPos.Angle) else oldMaxAngle - degreeDelta
            
            let oldAttenuation = gameState.Constants.Attenuation
            let attenuation = oldAttenuation + 0.05

            let oldSamples = gameState.Constants.Samples
            let samples = oldSamples + 2

            let constants = { gameState.Constants with MaxAngle = maxAngle; Attenuation = attenuation; Samples = samples}
            
            (log gameState.GameTick) <| sprintf "MaxAngle set to %f (was %f)" (constants.MaxAngle / System.Math.PI * 180.0) (oldMaxAngle / System.Math.PI * 180.0)
            (log gameState.GameTick) <| sprintf "Attenuation set to %f (was %f)" constants.Attenuation oldAttenuation
            (log gameState.GameTick) <| sprintf "Samples set to %d (was %d)" constants.Samples oldSamples

            //Update targetVelocities to reduce the target velocity for these piece.

            gameState.TargetVelKeys
            |> List.filter (fun key -> key.PId = gameState.CarPos.PieceId)
            |> List.iter (fun key -> 
                let v = gameState.TargetVels.[key]
                gameState.TargetVels.[key] <- { v with Velocity=v.Velocity - 0.5 })

            { gameState with Crashed = true; Constants = constants; } 

        | _ -> gameState
    
    let handleSpawn gameState = 
        match gameState.MessageReceived with 
        | Spawn carId when carId = gameState.YourCar -> { gameState with Crashed = false } 
        | _ -> gameState
            
    let updateCarVelocities carPos gameState = 
        let getTelemetry vel0 angVel0 pos1 pos0 =
            let angularVelocity = pos1.Angle - pos0.Angle

            let velocity = 
                if pos1.PieceId = pos0.PieceId then 
                    pos1.DistInPiece - pos0.DistInPiece
                else
                    let prevPiece = gameState.Race.Track.Pieces.Item(pos0.PieceId)
                    let wasNotSwitchedLastPiece = pos0.StartLane = pos0.EndLane

                    if wasNotSwitchedLastPiece
                    then
                        let lane = pos0.EndLane
                        let length = prevPiece.Lanes.[lane].Length
                        pos1.DistInPiece + (length - pos0.DistInPiece)
                    else
                        let lane1len = prevPiece.Lanes.[pos0.StartLane].Length
                        let lane2len = prevPiece.Lanes.[pos0.EndLane].Length

                        let fiddleFactor = 
                            match prevPiece.Spec with
                            | Straight _ -> 2.0602749929337278 // Value found by experimentation. close enough on straight
                            | Bend _ -> 2.50966780226329827 // Value found by experimentation. close enough on bend

                        let guessLength = fiddleFactor + ((lane1len + lane2len) / 2.) 
                        pos1.DistInPiece + (guessLength - pos0.DistInPiece)

            let acc = velocity - vel0
            let angAcc = angularVelocity - angVel0

            let distInLap = 
                let distBeforeCurrentPiece =              
                    gameState.Race.Track.Pieces
                    |> Seq.take pos1.PieceId
                    |> Seq.map (fun piece -> piece.Lanes |> Seq.maxBy (fun l -> l.Length))
                    |> Seq.map (fun l -> l.Length)
                    |> Seq.sum

                pos1.DistInPiece + distBeforeCurrentPiece 
                            
            { DistInLap=distInLap; Vel=velocity; Acc=acc; 
              Ang=pos1.Angle; AngVel=angularVelocity; AngAcc=angAcc; 
              Throttle = 0.0; CarPos = pos1}

        let appendNewTelemetry carPos = 

            let tel = gameState.Telemetry 
            let vel0,angVel0 = tel.Vel, tel.AngVel
            
            match gameState.Crashed with 
            | false -> (getTelemetry vel0 angVel0 carPos tel.CarPos) :: gameState.TelemetryHistory
            | _ -> gameState.TelemetryHistory

        let telemetryHistory = appendNewTelemetry carPos
        { gameState with TelemetryHistory = telemetryHistory }

    let updateTargetVelocities gameState = 

        let samples = 3

        match gameState.TelemetryHistory with 
        | t :: _ when gameState.TelemetryHistory.Length > samples -> 

            let generateTargetVel tv = 
                let ts = gameState.TelemetryHistory |> Seq.take samples

                let maxAngle = ts |> Seq.map (fun t -> abs t.Ang) |> Seq.max

                let angleTooGreat,ticks = isAngleGoingToGoOverMaxAngle (log gameState.GameTick) gameState
                
                match angleTooGreat,maxAngle with
                | true, _ -> 
                    let reduction = 0.005 * float (gameState.Constants.Samples + 1 - ticks)
                    { tv with Velocity=tv.Velocity - reduction }
                | _, x when x > gameState.Constants.MaxAngle -> { tv with Velocity=tv.Velocity - 0.04 }
                | _, x when t.Ang < gameState.Constants.MaxAngle ->  
                    if t.Vel + 0.1 > tv.Velocity 
                    then { tv with Velocity=tv.Velocity + 0.04 }
                    else tv
                | _ -> tv

            gameState.TargetVels.Keys
            |> Seq.toList
            |> List.filter (fun key -> key.PId = gameState.CarPos.PieceId)
            |> List.iter (fun key -> 
                let v = gameState.TargetVels.[key]
                gameState.TargetVels.[key] <- generateTargetVel v)

        | _ -> ()

    let updateConstants gameState = 
        //Only want to do this when there are 2 velocities. No more, no less.

        let ts = gameState.TelemetryHistory
        match gameState.Constants.LinearConstantsFound,ts with 
        | false, t2::t1::t0::_ when t1.Vel > 0.00000001 -> 
            let throttle0, throttle1 = 1.0, 1.0 //Assumed
            let power = t1.Vel / throttle0 
            let friction = t2.Vel / t1.Vel - throttle1
            (log gameState.GameTick) <| sprintf "Constants calculated: power=%f friction=%f" power friction
            let constants = { gameState.Constants with Power = power; Friction = friction; LinearConstantsFound=true }
            
            { gameState with Constants = constants }
        | _ -> gameState

    let updateCarPositionHistory gameState =
        
        match gameState.Crashed,gameState.MessageReceived with 
        | false,CarPositions cps ->
            
            let otherCars = cps |> Seq.filter (fun cp -> cp.CarId <> gameState.YourCar) |> Seq.map (fun cp -> cp.CarId,cp) |> Map.ofSeq

            let cp = cps |> Seq.filter (fun cp -> cp.CarId = gameState.YourCar) |> Seq.exactlyOne 
            
            let gameState = { gameState with CarPositionHistory = cp :: gameState.CarPositionHistory; OtherCars = otherCars }
            let gameState = updateCarVelocities cp gameState
            updateTargetVelocities gameState
            updateConstants gameState

        | _ -> gameState

    let updateLapTimes gameState = 
        match gameState.MessageReceived with 
        | LapFinished lapTime ->
            
            let laps = 
                match gameState.LapTimes.TryFind(lapTime.CarId) with 
                | Some laps -> lapTime.Time :: laps
                | _ -> [lapTime.Time]

            { gameState with LapTimes = gameState.LapTimes |> Map.add lapTime.CarId laps }
            
        | _ -> gameState
    
    let updateTurbo gameState =
        let updateActiveTurbo activeTurbo = 
            match activeTurbo with
            | _,ticks when ticks <= 0 -> None
            | turboSpec,ticks -> Some (turboSpec, ticks-1)

        match gameState.MessageReceived, gameState.TurboStatus.ActiveTurbo with 
        | TurboAvailable availableTurbo, None -> 
            let availableTurbo = if gameState.Crashed then gameState.TurboStatus.AvailableTurbo else Some availableTurbo
            let turboStatus = { gameState.TurboStatus with AvailableTurbo=availableTurbo }
            { gameState with TurboStatus=turboStatus } 
        | TurboAvailable availableTurbo, Some activeTurbo -> 
            let availableTurbo = if gameState.Crashed then gameState.TurboStatus.AvailableTurbo else Some availableTurbo
            let turboStatus = { AvailableTurbo = availableTurbo; ActiveTurbo = updateActiveTurbo activeTurbo }
            { gameState with TurboStatus=turboStatus } 
        | _, Some activeTurbo -> 
            let turboStatus = { gameState.TurboStatus with ActiveTurbo = updateActiveTurbo activeTurbo }
            { gameState with TurboStatus=turboStatus } 
        | _ -> gameState

    let recordSwitchRequested messageSent gameState = 
        match gameState.SwitchRequested, messageSent with
        | Some pieceId, _ when pieceId = gameState.CarPos.PieceId -> { gameState with SwitchRequested = None }
        | _, Switch (_,pieceId) -> { gameState with SwitchRequested = Some pieceId } 
        | _ -> gameState

    let recordTurboEngaged messageSent gameState =
        match messageSent, gameState.TurboStatus.AvailableTurbo with
        | EngageTurbo, Some turboSpec -> 
            { gameState with TurboStatus = { AvailableTurbo = None; ActiveTurbo = Some(turboSpec, turboSpec.Ticks) } }
        | _ -> gameState

    let recordThrottle messageSent gameState =
        match messageSent, gameState.TelemetryHistory with
        | Throttle t, latest :: previous -> 
            let updated = { latest with Throttle = t }
            { gameState with TelemetryHistory = updated :: previous }
        | _ -> gameState

    let rec gameLoop gameState (cts:CancellationTokenSource) = 

        let gameState = 
            match reader.ReadLine(), cts.IsCancellationRequested with
            | null,_ -> None
            | _, true -> 
                let gameState = { gameState with MessageReceived = GameEnd [] }
                updateHandler gameState
                None
            | msg,_ ->           
                //let sw = new System.Diagnostics.Stopwatch()

                try 
                    //sw.Restart()
                    let message, gt = parseMessage msg gameState.GameTick
                    let log = log gt

                    let gameState = 
                        { gameState with MessageReceived = message; GameTick = gt }
                        |> handleYourCar
                        |> handleRaceData
                        |> handleCrash
                        |> handleSpawn
                        |> updateCarPositionHistory 
                        |> updateLapTimes
                        |> updateTurbo

                    let messageToSend = aiHandler log (GameState gameState)

                    match messageToSend with
                    | NoReply -> ()
                    | msg -> send (createMessage msg gt)

                    let gameState = 
                        gameState 
                        |> recordSwitchRequested messageToSend
                        |> recordTurboEngaged messageToSend 
                        |> recordThrottle messageToSend

                    //Do generic logging and update UI after we've finished responding to the message
                    logGenericMessages log message
                    updateHandler gameState

                    //sw.Stop()
                    //System.Diagnostics.Debug.WriteLine ("MessageLoop {0}ms",sw.ElapsedMilliseconds)

                    Some gameState
                with
                | ex -> log gameState.GameTick ("Exception occurred " + ex.Message)
                        Some gameState
 
        match gameState with 
        | None -> ()
        | Some gs -> gameLoop gs cts

    member this.start joinMsg runSynchronously cts = 

        createMessage joinMsg -1 |> send
        let initialGameState = match GameState.Default with GameState gs -> gs

        if runSynchronously then
            gameLoop initialGameState cts
        else 
            Task.Factory.StartNew (fun () -> gameLoop initialGameState cts) |> ignore
