﻿module DaveAI

open Messaging

let mutable targetVelocity = Some 6.55

let useTurbo = 
    false

let sendSwitch :Direction option = 
    None 

let throttleAmount = 
    0.5

let getMessageToSend log (GameState state) =
    match useTurbo with
    | true -> EngageTurbo
    | false -> 
        match sendSwitch with
        | Some (direction) -> Switch (direction, -1)
        | None ->
            let telemetry = state.TelemetryHistory

            let throttle =

                if telemetry.Length < 2 then
                    Some 1.
                else
                    let velocity = telemetry.Head.Vel
                
                    match targetVelocity with
                    | Some target ->
                        if velocity < 0. then None
                        else (target - velocity * state.Constants.Friction) / state.Constants.Power |> min 1. |> max 0. |> Some
                    | _ -> Some 0.7
                

            match throttle with
            | Some throttle -> Throttle throttle
            | _ -> Ping

let aiHandler log (GameState state as gameState) = 
    
    match state.MessageReceived with
        | CarPositions _ -> 
            let distInPiece,angle = state.CarPos.DistInPiece, state.CarPos.Angle

            let velocity = state.Telemetry.Vel
            
            log (sprintf "0,%12.10f,%12.10f,%12.10f,0" distInPiece velocity angle)
            getMessageToSend log gameState

        | Race r -> log (sprintf "%A" r); NoReply
        | _ -> NoReply
