﻿[<AutoOpen>]
module AI

open System.Collections.Generic
open Messaging

let nextPieces state = 

    let pieces = state.Race.Track.Pieces

    let laps = match state.Race.Session with
                | QuickRace x -> x.LapCount - 1
                | FullRace x -> x.LapCount - 1
                | _ -> 1000

    let isLastLap = state.CarPos.Lap = laps

    let numberOfPiecesToTest = if isLastLap then min 10 (pieces.Length - state.CarPos.PieceId) else 10

    let pieceList = 
        seq { yield! pieces; yield! pieces } 
        |> Seq.skip (state.CarPos.PieceId)
        |> Seq.take (max numberOfPiecesToTest 1)

    let currentPiece = pieceList |> Seq.take 1 |> Seq.exactlyOne
    let nextPieces = pieceList |> Seq.skip 1

    let lengthInCurrentPiece = 0. - state.CarPos.DistInPiece

    seq {
        //TODO not happy about using a ref cell here - do it immutably
        let dist = ref lengthInCurrentPiece
        yield (currentPiece, lengthInCurrentPiece)
        for piece in pieceList do
            let shortestLane = piece.Lanes |> Seq.minBy (fun tl -> tl.Length)
            dist := dist.Value + shortestLane.Length
            yield (piece,dist.Value)
    }

let isAngleGoingToGoOverMaxAngle = 
    let cache = new Dictionary<_,_>()

    let isAngleGoingToGoOverMaxAngle' log gameState =
        let ticksToStraight = 
        
            let currentPiece = gameState.Race.Track.Pieces.Item(gameState.CarPos.PieceId)
            let currentBendAngle = match currentPiece.Spec with Bend b -> b.Angle | _ -> 0.

            let nextPs = (nextPieces gameState)

            let isStraightOrOppositeBend (tp,_) = 
                match tp.Spec with 
                | Straight _ -> true 
                | Bend b when currentBendAngle > 0. -> b.Angle < 0.
                | Bend b when currentBendAngle < 0. -> b.Angle > 0.
                | _ -> false

            let nextStraight = nextPs |> Seq.skipWhile isStraightOrOppositeBend |> Seq.tryFind isStraightOrOppositeBend
            let vel = gameState.Telemetry.Vel
        
            match nextStraight with 
            | Some (tp,d) -> 
                match tp.Spec with 
                | Straight s -> 
                    let ticks = max (int ((d - s.Length)/vel) + 1) 1
                    if ticks < gameState.Constants.Samples then ticks else gameState.Constants.Samples
                | _ -> gameState.Constants.Samples
            | _ -> gameState.Constants.Samples

        let simulateNextTelemetry tel ticksToStraight = 
            let ang = tel.Ang + tel.AngVel
            let angVel = tel.AngVel + tel.AngAcc
            let attenuation = gameState.Constants.Attenuation
            let attenuation = if ticksToStraight < 0 then attenuation * attenuation else attenuation
            let angAcc = tel.AngAcc * attenuation
            { tel with Ang=ang; AngVel=angVel; AngAcc=angAcc; }

        let rec testIfAngleIsGoingToExceedMaxAngle tel ticksToStraight samplesLeft =
            match samplesLeft with
            | x when x < 0 -> false, gameState.Constants.Samples
            | x -> 
                let tel = simulateNextTelemetry tel ticksToStraight
                match (abs tel.Ang) > gameState.Constants.MaxAngle with 
                | true -> true,samplesLeft
                | _ -> testIfAngleIsGoingToExceedMaxAngle tel (ticksToStraight-1) (samplesLeft-1)

        testIfAngleIsGoingToExceedMaxAngle gameState.Telemetry ticksToStraight gameState.Constants.Samples 
        
    (fun log gameState -> 
        
        match cache.TryGetValue(gameState.GameTick) with
        | true, v -> v
        | _ -> 
            let v = isAngleGoingToGoOverMaxAngle' log gameState
            cache.Add(gameState.GameTick, v)
            v
    )

let useTurbo state = 

    let pieces = state.Race.Track.Pieces

    let getOffsetPiece n = 
        let i = state.CarPos.PieceId + n
        let i = if i < pieces.Length then i else i - pieces.Length
        i

    let isUnderTargetVel (pieceId) = 
        let v = state.TargetVels |> Seq.filter (fun kvp -> kvp.Key.PId = pieceId) |> Seq.map (fun kvp -> kvp.Value) |> Seq.minBy (fun v -> v.Velocity)
        v.Velocity > state.Telemetry.Vel + 3.0

    match state.TurboStatus.AvailableTurbo with 
    | None -> false
    | Some _ -> Seq.init 3 id |> Seq.map (getOffsetPiece >> isUnderTargetVel) |> Seq.forall id

let getSwitchDirection log (state:GameStateSpec) = 
    
    let pieces = state.Race.Track.Pieces
    let currentLane = state.CarPos.EndLane

    let bestLap carId = 
        match state.LapTimes.TryFind carId with
        | Some ls -> (ls |> Seq.minBy (fun l -> l.LapMs)).LapTicks |> float
        | None -> 10000.0

    let myBestLap = bestLap state.YourCar

    let slowCarsFactorForLane n myPieceId = 
        let otherCars = 
            state.OtherCars
            |> Map.toSeq 

        otherCars |> Seq.filter (fun (k,o) -> 
                            o.EndLane = n 
                            && o.PieceId >= myPieceId - 1 
                            && o.PieceId - myPieceId < 5)
                  |> Seq.sumBy (fun (k,o) -> max 0.0 (bestLap k - myBestLap))

    let calculateLengthToNextSwitch laneId nextPieceId currentLaneId = 
        let laneInfos = seq { yield! pieces; yield! pieces } |> Seq.skip nextPieceId
        
        let laneInfosUntilNextSwitch = laneInfos |> Seq.takeWhile (fun tp -> not tp.IsSwitch)

        let slowCarFudge = slowCarsFactorForLane laneId nextPieceId

        let totalLength = laneInfosUntilNextSwitch |> Seq.map (fun tp -> tp.Lanes.[laneId].Length)|> Seq.sum
        if laneId = currentLaneId 
        then totalLength + slowCarFudge
        else totalLength + 2. + slowCarFudge

    let decideIfWeShouldSwitch =
        let nextPieceId,nextPiece = match state.Race.Track.Pieces |> Seq.tryFind (fun tp -> tp.Id = state.CarPos.PieceId + 1) with
                                    | Some i -> (state.CarPos.PieceId + 1), i
                                    | None -> 0, pieces.Item(0)
        
        match nextPiece.IsSwitch with
        | false -> None, -1
        | true -> 
            let currentLane = nextPiece.Lanes.[currentLane]
   
            let laneOptions = match currentLane.LeftLane,currentLane.RightLane with 
                              | None,None -> [ currentLane.Lane.Index ]
                              | Some left, None -> [ left; currentLane.Lane.Index ]
                              | None, Some right -> [ currentLane.Lane.Index; right ]
                              | Some left, Some right -> [ left; currentLane.Lane.Index; right ]
            
            let shortestLane = laneOptions 
                               |> List.map (fun id -> nextPiece.Lanes.[id])
                               |> List.minBy (fun l -> calculateLengthToNextSwitch l.Lane.Index (nextPieceId+1) currentLane.Lane.Index)
            
            match shortestLane.Lane.Index,currentLane.Lane.Index with
            | s,c when s < c -> Some (Left), nextPieceId
            | s,c when s > c -> Some (Right), nextPieceId
            | _ -> None, -1

    match state.SwitchRequested with
    | Some _ -> None, -1
    | _ -> decideIfWeShouldSwitch
    

let getThrottle log state = 
    
    let toDegrees radians = radians * 180. / System.Math.PI
    let toRadians degrees = degrees * System.Math.PI / 180.

    let targetVelocity (state:GameStateSpec) = 
    
        let getTargetVelocity (trackPiece:TrackPiece) =

            let currentLane = trackPiece.Lanes.[state.CarPos.EndLane]

            let laneOptions = match currentLane.LeftLane,currentLane.RightLane with 
                              | None,None -> [ currentLane.Lane.Index ]
                              | Some left, None -> [ left; currentLane.Lane.Index ]
                              | None, Some right -> [ currentLane.Lane.Index; right ]
                              | Some left, Some right -> [ left; currentLane.Lane.Index; right ]

            let lookUpTargetVel id startLane endLane = 
                let key = { PId=id; StartLn=startLane; EndLn=endLane }
                match state.TargetVels.TryGetValue(key) with
                | true,v -> v.Velocity
                | _ ->
                    let lanes = state.Race.Track.Lanes
                    let laneOffset = lanes.[state.CarPos.EndLane].DistFromCentre
                
                    match trackPiece.Spec with 
                    | Bend b -> 
                        let trueR = if b.Angle > 0. then b.Radius - laneOffset else b.Radius + laneOffset
                        System.Math.Sqrt(trueR) * 0.65
                    | _ -> 30.0

            let targetVelocities = laneOptions |> List.map (fun laneId -> lookUpTargetVel trackPiece.Id state.CarPos.EndLane laneId)
            
            targetVelocities |> Seq.min

        let distNeededToSlowToTargetVelocity vInitial vTarget = 

            let rec distNeededToSlowToTargetVelocity' vCurrent vTarget distSoFar = 
                if vCurrent <= (max vTarget 1.0)
                then distSoFar + vCurrent
                else 
                    let vNext = vCurrent * state.Constants.Friction
                    distNeededToSlowToTargetVelocity' vNext vTarget (distSoFar + vCurrent)
            
            let distanceInNextTickIfWeDoFullThrottlNow = 
                (vInitial * state.Constants.Friction + 1.0 * state.Constants.Power)
            distNeededToSlowToTargetVelocity' vInitial vTarget distanceInNextTickIfWeDoFullThrottlNow

        let requiredVelocity vInitial (trackPiece, distToPiece) = 
            let vTarget = getTargetVelocity trackPiece
            let distNeeded = distNeededToSlowToTargetVelocity vInitial vTarget
            let distAvailable = max distToPiece 0.0
            if distNeeded > distAvailable then vTarget else 100.0

        let vInitial = state.Telemetry.Vel

        let minVelocity minSoFar trackPiece = 
            let requiredVelocity = requiredVelocity vInitial trackPiece 
            min minSoFar requiredVelocity

        let maxSafeVelocity = (nextPieces state) |> Seq.fold minVelocity 100.0

        maxSafeVelocity

    match state.GameTick with
    | x when state.Constants.LinearConstantsFound = false -> 1.0 // to get power and friction
    | _ ->
        
        let maxAngularVelocityThreshold = 
            match state.CarPos.Angle with
            | a when a > toRadians 40. -> 0.01
            | a when a > toRadians 30. -> 0.02
            | _ -> 0.08

        let shouldUseEmergencyBrake = 
            let angleTooGreat,_ = isAngleGoingToGoOverMaxAngle log state
            let maxAngExceeded = (abs state.CarPos.Angle) > state.Constants.MaxAngle

            match angleTooGreat,maxAngExceeded with
            | true,_ -> ()
            | _,true -> log <| sprintf "Emergency brake Angl: angl=%f" (toDegrees (abs state.CarPos.Angle))
            | _ -> ()

            angleTooGreat || maxAngExceeded

        if shouldUseEmergencyBrake then 0.0
        else (targetVelocity state - state.Telemetry.Vel * state.Constants.Friction) / state.Constants.Power |> min 1. |> max 0.

let getMessageToSend log (state:GameStateSpec) =

    let success, t = System.Double.TryParse(state.YourCar.Name)
    if success then Throttle t 
    else

        match useTurbo state with
        | true -> EngageTurbo
        | false -> 
            match getSwitchDirection log state with
            | Some (direction),pieceId -> Switch (direction,pieceId)
            | None,_ -> Throttle (getThrottle log state)


let basicAI log (GameState state) = 
    
    match state.MessageReceived, state.GameTick with
        | GameStart, _ -> getMessageToSend log state
        | CarPositions cps, t when t > 0 -> getMessageToSend log state
        | _ -> NoReply
