﻿#r "FSharp.Data"

#load "Domain.fs"
#load "Parser.fs"
#load "Messaging.fs"
#load "AI.fs"

open System
open System.Drawing

 
// *** Test data **********************************************************************************

let test_gameInit1 = """{"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"BodgerBob","color":"blue"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"a986bbf9-a4b4-4ad5-b455-b6063beed771"}"""
let test_gameInit2 = """{"msgType":"gameInit","data":{"race":{"track":{"id":"indianapolis","name":"Indianapolis","pieces":[{"length":100.0},{"radius":100,"angle":12.5,"switch":true},{"length":100.0,"switch":true},{"radius":200,"angle":22.5}],"lanes":[{"distanceFromCenter":-20,"index":0},{"distanceFromCenter":0,"index":1},{"distanceFromCenter":20,"index":2}],"startingPoint":{"position":{"x":-340.0,"y":-96.0},"angle":90.0}},"cars":[{"id":{"name":"Schumacher","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}},{"id":{"name":"Rosberg","color":"blue"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":30000,"quickRace":true}}}}"""

let test_carPositions = """{"msgType":"carPositions","data":[{"id":{"name":"Schumacher","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}},{"id":{"name":"Rosberg","color":"blue"},"angle":45.0,"piecePosition":{"pieceIndex":1,"inPieceDistance":20.0,"lane":{"startLaneIndex":0,"endLaneIndex":1},"lap":1}}],"gameId":"OIUHGERJWEOI","gameTick":0}"""

// *** Parse Tests ********************************************************************************

//
let race = match Messaging.parseMessage test_gameInit1 0 with Race r, gt -> r | _ -> failwith ("uhhhh")

let pieces = race.Track.Pieces |> Map.toList |> List.map snd



//Start of charting stuff
#load "../packages/FSharp.Charting.0.90.6/FSharp.Charting.fsx"

module Event = 
    /// An event which triggers on every 'n' triggers of the input event
//    let every n (ev:IEvent<_>) = 
//        let out = new Event<_>()
//        let count = ref 0 
//        ev.Add (fun arg -> incr count; if !count % n = 0 then out.Trigger arg)
//        out.Publish
//
//    /// An event which triggers on every event once at least 'n' samples are available, reporting the last 'n' samples
//    let window n (ev:IEvent<_>) = 
//        let out = new Event<_>()
//        let queue = System.Collections.Generic.Queue<_>()
//        ev.Add (fun arg -> queue.Enqueue arg; 
//                           if queue.Count >= n then 
//                                out.Trigger (queue.ToArray()); 
//                                queue.Dequeue() |> ignore)
//        out.Publish
//
    /// An event which triggers on every event, reporting at most the last 'n' samples
    let windowAtMost n (ev:IEvent<_>) = 
        let out = new Event<_>()
        let queue = System.Collections.Generic.Queue<_>()
        ev.Add (fun arg -> queue.Enqueue arg; 
                           out.Trigger (queue.ToArray()); 
                           if queue.Count >= n then 
                                queue.Dequeue() |> ignore)
        out.Publish
//
//    /// An event which triggers on every event, reporting samples from the given time window
//    let windowTimeInterval (interval:int) (ev:IEvent<System.DateTime * _>) = 
//        let out = new Event<_>()
//        let queue = System.Collections.Generic.Queue<_>()
//        ev.Add (fun arg -> queue.Enqueue arg; 
//                           while (System.DateTime.Now - fst (queue.Peek())).TotalMilliseconds > float interval  do
//                                queue.Dequeue() |> ignore
//                           out.Trigger (queue.ToArray()))
//        out.Publish
//
//    /// An event which triggers at regular intervals reporting the latest observed value of the given event
//    let sampled interval (ev:IEvent<_>) = 
//        let out = new Event<_>()
//        let latest = ref None
//        let timer = new System.Windows.Forms.Timer(Interval=interval, Enabled=true)
//        timer.Tick.Add (fun args -> match latest.Value with None -> () | Some x -> out.Trigger (System.DateTime.Now,x))
//        timer.Start()
//        ev.Add (fun arg -> latest := Some arg)
//        out.Publish

    /// An event which triggers at regular intervals reporting the real world time at each trigger
    let clock interval = 
        let out = new Event<_>()
        let timer = new System.Windows.Forms.Timer(Interval=interval, Enabled=true)
        timer.Tick.Add (fun args -> out.Trigger System.DateTime.Now)
        timer.Start()
        out.Publish
//
//    /// Cycle through the values at the given interval
//    let cycle interval values = 
//        let values = Seq.toArray values
//        let out = new Event<_>()
//        let timer = new System.Windows.Forms.Timer(Interval=interval, Enabled=true)
//        let count = ref 0 
//        timer.Tick.Add (fun args -> out.Trigger (values.[!count % values.Length]); incr count)
//        timer.Start()
//        out.Publish
//
//    let pairwise  (ev:IEvent<_>) = 
//        let out = new Event<_>()
//        let queue = System.Collections.Generic.Queue<_>()
//        ev.Add (fun arg -> queue.Enqueue arg; 
//                           if queue.Count >= 2 then 
//                                let elems = queue.ToArray()
//                                out.Trigger (elems.[0], elems.[1])
//                                queue.Dequeue() |> ignore)
//        out.Publish
//
open FSharp.Charting
open System
open System.Drawing
//
//let timeSeriesData = [ for x in 0 .. 99 -> (DateTime.Now.AddDays (float x),sin(float x / 10.0)) ]
//let rnd = new System.Random()
//let rand() = rnd.NextDouble()
//
//let form = new System.Windows.Forms.Form(Visible=true,TopMost=true)
//let incData = form.MouseMove |> Event.map (fun e -> e.Y) |> Event.sampled 30 
//let evData = form.MouseMove |> Event.map (fun e -> e.Y) |> Event.sampled 30 |> Event.windowTimeInterval 3000

let evData2 = 
    Event.clock 20
    |> Event.map (fun x -> (x, 10.0 + 20.0 * sin (float (x.Ticks / 4000000L))))
    |> Event.windowAtMost 100

LiveChart.FastLine(evData2,Name="Clock")
    .WithXAxis(Title = "abc")
    .WithYAxis(Title="def")

LiveChart.FastPoint(evData2,Name="Clock")