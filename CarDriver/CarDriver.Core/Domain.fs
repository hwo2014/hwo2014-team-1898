﻿[<AutoOpen>]
module Domain

open System.Collections.Generic

type Lane = { Index:int; DistFromCentre:float }
type TrackLane = { Lane:Lane; Length:float; LeftLane:int option; RightLane:int option }

type Straight = { Length:float; }
type Bend = { Angle:float; Radius:float }
type StraightOrBend = Straight of Straight | Bend of Bend

type TrackPiece = { Id:int; Spec:StraightOrBend; IsSwitch:bool; Lanes:TrackLane[] }

type Origin = { X:float; Y:float; A:float }
type Track = { Id:string; Name:string; Pieces:TrackPiece list; Lanes:Map<int,Lane>; Origin:Origin }

type CarId = { Name:string; Colour:string}
type Car = { Id:CarId; Width:float; Length:float; GuideFlagPos:float }

type RaceSpec = { LapCount:int; MaxLapTimeMs:int; }
type Session = | QuickRace of RaceSpec
               | FullRace of RaceSpec
               | Qualifying of int

type Race = { Track:Track; Cars:Map<CarId,Car>; Session:Session } 

type CarPosition = { CarId:CarId; Angle:float; Lap:int; PieceId:int; DistInPiece:float; StartLane:int; EndLane:int }

type LapTime = { Lap:int; LapTicks:int; LapMs:int }
type RaceTime = { Laps:int; RaceTicks:int; RaceMs:int }

type LapResult = { CarId:CarId; Time:LapTime; RaceTime:RaceTime } 
type RaceResult = { CarId:CarId; RaceTime:RaceTime option; BestLap:LapTime option }

type TurboSpec = { Factor:float; Ticks:int; Ms:int; }

type TurboStatus = { AvailableTurbo:TurboSpec option; ActiveTurbo: (TurboSpec * int) option }

type MessageResult =
    | Join
    | YourCar of CarId
    | Race of Race
    | GameStart
    | CarPositions of CarPosition[]
    | LapFinished of LapResult
    | TurboAvailable of TurboSpec
    | TurboStart of CarId
    | TurboEnd of CarId
    | Crash of CarId
    | Spawn of CarId
    | Dnf of CarId * string
    | Finish of CarId
    | GameEnd of RaceResult list
    | TournamentEnd
    | Unknown of string

type Direction = Left | Right

type MessageParams =
    | SimpleJoin of string * string
    | AdvancedJoin of string * string * string * int
    | Ping
    | Throttle of float
    | Switch of Direction * int
    | EngageTurbo
    | NoReply

type Telemetry = { DistInLap:float; Vel:float; Acc:float; Ang:float; AngVel:float; AngAcc:float; Throttle:float; CarPos:CarPosition }

type TargetVKey = { PId:int; StartLn:int; EndLn:int; }
type TargetVData = { Velocity:float; } 

type PhysicsConstants = { Power:float; Friction:float
                          MaxAngle:float; Attenuation:float
                          RadialImpulse:float; ConstantImpulse:float
                          Samples:int
                          LinearConstantsFound:bool; AngularConstantsFound:bool } 

type GameStateSpec = 
    { Race: Race
      YourCar: CarId

      GameTick:int
      MessageReceived: MessageResult

      CarPositionHistory: CarPosition list
      TelemetryHistory: Telemetry list

      LapTimes: Map<CarId,LapTime list>
      OtherCars: Map<CarId,CarPosition>

      TargetVels: Dictionary<TargetVKey,TargetVData>
      TargetVelKeys: TargetVKey list

      Constants: PhysicsConstants

      Crashed : bool
      TurboStatus:TurboStatus
      SwitchRequested: int option }
      
    member x.Telemetry = 
        match x.TelemetryHistory with
        | h :: _ -> h
        | _ -> 
            let defaultCarPos = { CarId=x.YourCar; Angle=0.0; Lap=0; PieceId=0; DistInPiece=0.0; StartLane=0; EndLane=0 }
            { DistInLap=0.0; Vel=0.0; Acc=0.0; Ang=0.0; AngVel=0.0; AngAcc=0.0; Throttle=0.0; CarPos=defaultCarPos }
   
    member x.CarPos = x.Telemetry.CarPos

type GameState = GameState of GameStateSpec
    with
        static member Default = 
            GameState {
                Race = { Track = { Id = ""; Name = ""; Pieces = []; Lanes = Map.empty; Origin = { X=650.; Y=300.; A=0. } }
                         Cars = Map.empty
                         Session = FullRace { LapCount = 3; MaxLapTimeMs = 10000 } }
                
                YourCar = { Name = ""; Colour = "" }

                GameTick = 0
                MessageReceived = MessageResult.Unknown ""

                CarPositionHistory = []
                TelemetryHistory = []
                LapTimes = Map.empty
                OtherCars = Map.empty

                TargetVels = new Dictionary<_,_>()
                TargetVelKeys = []

                Constants = { Power = 0.2; Friction = 0.98; MaxAngle= (58.5 * System.Math.PI / 180.0); 
                              Attenuation = 0.9; Samples = 13
                              RadialImpulse = 0.00925600625653143; ConstantImpulse = -0.00523598787185202
                              LinearConstantsFound = false; AngularConstantsFound = false }
                
                Crashed = false
                TurboStatus = { AvailableTurbo = None; ActiveTurbo = None }
                SwitchRequested = None }
