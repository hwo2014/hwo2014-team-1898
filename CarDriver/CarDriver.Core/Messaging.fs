﻿module Messaging

open FSharp.Data
open FSharp.Data.JsonExtensions 

open Parser

let parseMessage input lastGameTick =
    let json = JsonValue.Parse input
    let msgType = json.TryGetProperty "msgType"
    let msgData = json.TryGetProperty "data"
    let gameId = json.TryGetProperty "gameId"
    let currentGameTick = json.TryGetProperty "gameTick" 
                          |> Option.fold (fun _ v -> v.AsInteger()) lastGameTick

    let message = 
        match msgType,msgData with
        | Some (JsonValue.String "join"), Some d -> Join
        | Some (JsonValue.String "yourCar"), Some d -> parseYourCar d
        | Some (JsonValue.String "gameInit"), Some d -> Race (parseGameInit d)
        | Some (JsonValue.String "gameStart"), _ ->  GameStart
        | Some (JsonValue.String "carPositions"), Some d -> CarPositions (parseCarPositions d)
        | Some (JsonValue.String "lapFinished"), Some d -> parseLapFinished d
        | Some (JsonValue.String "turboAvailable"), Some d -> parseTurboAvailable d
        | Some (JsonValue.String "turboStart"), Some d -> parseTurboStart d
        | Some (JsonValue.String "turboEnd"), Some d -> parseTurboEnd d
        | Some (JsonValue.String "crash"), Some d -> parseCrash d
        | Some (JsonValue.String "spawn"), Some d -> parseSpawn d
        | Some (JsonValue.String "dnf"), Some d -> parseDnf d
        | Some (JsonValue.String "finish"), Some d -> parseFinish d
        | Some (JsonValue.String "gameEnd"), Some d -> parseGameFinished d
        | Some (JsonValue.String "tournamentEnd"), _ -> TournamentEnd
        | msgName,msgData -> Unknown input

    message, currentGameTick

let createMessage messageParams gt = 

    let jFloat key f = key,JsonValue.Float f
    let jString key s = key,JsonValue.String s
    let jNumber key n = key,JsonValue.Number (decimal n)
    let jRecord key ps = key,JsonValue.Record ps

    let gameTick = jNumber "gameTick" gt
    let msgType t = jString "msgType" t
        
    let fields = 
        match messageParams with
        | NoReply -> [||]
        | Ping -> [| msgType "ping" |]

        | Throttle t -> 
            let data = jFloat "data" t
            [| msgType "throttle"; data; gameTick |]

        | Switch (dir,_) -> 
            let data = jString "data" (sprintf "%A" dir)
            [| msgType "switchLane"; data; gameTick |]

        | EngageTurbo -> 
            let data = jString "data" "Turbo Power: Engage!"
            [| msgType "turbo"; data; gameTick |]

        | SimpleJoin (name,key) -> 
            let name = jString "name" name
            let key = jString "key" key
            let data = jRecord "data" [| name; key |]
            [| msgType "join"; data |]

        | AdvancedJoin (name,key,track,carCount) -> 
            let name = jString "name" name
            let key = jString "key" key
            let botId = jRecord "botId" [| name; key |]
            let track = jString "trackName" track
            let carCount = jNumber "carCount" carCount
            let data = jRecord "data" [| botId; track; carCount |]
            [| msgType "joinRace"; data |]

    let json = JsonValue.Record fields
    json.ToString(JsonSaveOptions.DisableFormatting)
