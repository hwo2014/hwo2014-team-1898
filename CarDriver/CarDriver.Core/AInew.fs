﻿[<AutoOpen>]
module AInew

open System.Collections.Generic
open Messaging

let nextPieces state = 

    let pieces = state.Race.Track.Pieces

    let laps = match state.Race.Session with
                | QuickRace x -> x.LapCount - 1
                | FullRace x -> x.LapCount - 1
                | _ -> 1000

    let isLastLap = state.CarPos.Lap = laps

    let numberOfPiecesToTest = if isLastLap then min 7 (pieces.Length - state.CarPos.PieceId) else 7

    let pieceList = 
        seq { yield! pieces; yield! pieces } 
        |> Seq.skip (state.CarPos.PieceId)
        |> Seq.take (max numberOfPiecesToTest 1)

    let currentPiece = pieceList |> Seq.take 1 |> Seq.exactlyOne
    let nextPieces = pieceList |> Seq.skip 1

    let lengthInCurrentPiece = 0. - state.CarPos.DistInPiece

    seq {
        //TODO not happy about using a ref cell here - do it immutably
        let dist = ref lengthInCurrentPiece
        yield (currentPiece, lengthInCurrentPiece)
        for piece in pieceList do
            let shortestLane = piece.Lanes |> Seq.minBy (fun tl -> tl.Length)
            dist := dist.Value + shortestLane.Length
            yield (piece,dist.Value)
    }

let isAngleGoingToGoOverMaxAngle = 
    let cache = new Dictionary<_,_>()

    let isAngleGoingToGoOverMaxAngle' log gameState =
//        let ticksToStraight = 
//        
//            let currentPiece = gameState.Race.Track.Pieces.Item(gameState.CarPos.PieceId)
//            let currentBendAngle = match currentPiece.Spec with Bend b -> b.Angle | _ -> 0.
//
//            let nextPs = (nextPieces gameState)
//
//            let isStraightOrOppositeBend (tp,_) = 
//                match tp.Spec with 
//                | Straight _ -> true 
//                | Bend b when currentBendAngle > 0. -> b.Angle < 0.
//                | Bend b when currentBendAngle < 0. -> b.Angle > 0.
//                | _ -> false
//
//            let nextStraight = nextPs |> Seq.skipWhile isStraightOrOppositeBend |> Seq.tryFind isStraightOrOppositeBend
//            let vel = gameState.Telemetry.Vel
//        
//            match nextStraight with 
//            | Some (tp,d) -> 
//                match tp.Spec with 
//                | Straight s -> 
//                    let ticks = max (int ((d - s.Length)/vel) + 1) 1
//                    
//                    min ticks gameState.Constants.Samples
//                | _ -> gameState.Constants.Samples
//            | _ -> gameState.Constants.Samples

        let calculateImpulse prevPieceId pieceId laneId v =

            let calculateSubImpulse pieceId =

                let piece = gameState.Race.Track.Pieces.[pieceId]
                let offset = piece.Lanes.[laneId].Lane.DistFromCentre 

                match piece.Spec with
                | Bend b ->
                    if b.Angle < 0.0 then
                        -1. * (gameState.Constants.RadialImpulse / System.Math.Sqrt(b.Radius + offset) * v * v + gameState.Constants.ConstantImpulse * v)
                    else
                        gameState.Constants.RadialImpulse / System.Math.Sqrt(b.Radius - offset) * v * v + gameState.Constants.ConstantImpulse * v
                | Straight s -> 0.

            let impulse1 = -1. * calculateSubImpulse prevPieceId
            let impulse2 = calculateSubImpulse pieceId

            impulse1 + impulse2

        let simulateNextTelemetry tel prevTel=
            let acc = tel.Acc * gameState.Constants.Friction + prevTel.Throttle * gameState.Constants.Power
            let vel = tel.Vel + acc
            let distInPiece = tel.CarPos.DistInPiece + vel
            let distInLap = (tel.DistInLap + vel) % (gameState.Race.Track.Pieces |> Seq.sumBy( fun p -> (p.Lanes |> Seq.maxBy(fun l -> l.Length)).Length ) )
            
            let distInPiece, pieceId =
                let pieceLength = gameState.Race.Track.Pieces.[tel.CarPos.PieceId].Lanes.[tel.CarPos.EndLane].Length + 2.5
                if distInPiece > pieceLength then
                    distInPiece - pieceLength, (tel.CarPos.PieceId + 1) % gameState.Race.Track.Pieces.Length
                else
                    distInPiece, tel.CarPos.PieceId

            let impulse = calculateImpulse prevTel.CarPos.PieceId tel.CarPos.PieceId prevTel.CarPos.EndLane tel.Vel
            
            let angAcc = tel.AngAcc+ impulse
            let angVel = tel.AngVel + angAcc
            let ang = tel.Ang + angVel
     
            { tel with  DistInLap = distInLap
                        CarPos = { tel.CarPos with DistInPiece = distInPiece; PieceId = pieceId }; Vel=vel; Acc = acc
                        Throttle = 0.; Ang=ang; AngVel=angVel; AngAcc=angAcc }

        let rec testIfAngleIsGoingToExceedMaxAngle tel prevTel samplesLeft =
            match samplesLeft with
            | x when x < 0 -> false,gameState.Constants.Samples
            | x -> 
                let nextTel = simulateNextTelemetry { tel with Throttle = 1. } { prevTel with Throttle=1. }
                if (samplesLeft = gameState.Constants.Samples) then
                    log <| sprintf "%0.15f,%0.15f,%0.15f" nextTel.Ang nextTel.AngVel nextTel.AngAcc

                let isAngleOver = (abs nextTel.Ang) > gameState.Constants.MaxAngle
                if isAngleOver then 
                    true, gameState.Constants.Samples-samplesLeft
                else 
                    testIfAngleIsGoingToExceedMaxAngle nextTel tel (samplesLeft-1)

        //Always expect to have at least one element, because it is initialised this way
        let tel, prevTel = 
            match gameState.TelemetryHistory with
            | a::b::_ -> a,b
            | a::[] -> a,a
            | _ -> failwith "hiya!"

        testIfAngleIsGoingToExceedMaxAngle tel prevTel gameState.Constants.Samples 
        
    (fun log gameState -> 
        
        match cache.TryGetValue(gameState.GameTick) with
        | true, v -> v
        | _ -> 
            let v = isAngleGoingToGoOverMaxAngle' log gameState
            cache.Add(gameState.GameTick, v)
            v
    )

let useTurbo state = 

    let pieces = state.Race.Track.Pieces

    let getOffsetPiece n = 
        let i = state.CarPos.PieceId + n
        let i = if i < pieces.Length then i else i - pieces.Length
        i

    let isUnderTargetVel (pieceId) = 
        let v = state.TargetVels |> Seq.filter (fun kvp -> kvp.Key.PId = pieceId) |> Seq.map (fun kvp -> kvp.Value) |> Seq.minBy (fun v -> v.Velocity)
        v.Velocity > state.Telemetry.Vel + 3.0

    match state.TurboStatus.AvailableTurbo with 
    | None -> false
    | Some _ -> Seq.init 3 id |> Seq.map (getOffsetPiece >> isUnderTargetVel) |> Seq.forall id

let getSwitchDirection log (state:GameStateSpec) = 
    
    let pieces = state.Race.Track.Pieces
    let currentLane = state.CarPos.EndLane

    let calculateLengthToNextSwitch laneId nextPieceId currentLaneId = 
        let laneInfos = seq { yield! pieces; yield! pieces } |> Seq.skip nextPieceId
        
        let laneInfosUntilNextSwitch = laneInfos |> Seq.takeWhile (fun tp -> not tp.IsSwitch)

        let totalLength = laneInfosUntilNextSwitch |> Seq.map (fun tp -> tp.Lanes.[laneId].Length)|> Seq.sum
        if laneId = currentLaneId 
        then totalLength
        else totalLength + 2.

    let decideIfWeShouldSwitch =
        let nextPieceId,nextPiece = match state.Race.Track.Pieces |> Seq.tryFind (fun tp -> tp.Id = state.CarPos.PieceId + 1) with
                                    | Some i -> (state.CarPos.PieceId + 1), i
                                    | None -> 0, pieces.Item(0)
        
        match nextPiece.IsSwitch with
        | false -> None, -1
        | true -> 
            let currentLane = nextPiece.Lanes.[currentLane]
   
            let laneOptions = match currentLane.LeftLane,currentLane.RightLane with 
                              | None,None -> [ currentLane.Lane.Index ]
                              | Some left, None -> [ left; currentLane.Lane.Index ]
                              | None, Some right -> [ currentLane.Lane.Index; right ]
                              | Some left, Some right -> [ left; currentLane.Lane.Index; right ]
            
            let shortestLane = laneOptions 
                               |> List.map (fun id -> nextPiece.Lanes.[id])
                               |> List.minBy (fun l -> calculateLengthToNextSwitch l.Lane.Index (nextPieceId+1) currentLane.Lane.Index)
            
            match shortestLane.Lane.Index,currentLane.Lane.Index with
            | s,c when s < c -> Some (Left), nextPieceId
            | s,c when s > c -> Some (Right), nextPieceId
            | _ -> None, -1

    match state.SwitchRequested with
    | Some _ -> None, -1
    | _ -> decideIfWeShouldSwitch
    

let getThrottle log state = 
    
    let toDegrees radians = radians * 180. / System.Math.PI
    let toRadians degrees = degrees * System.Math.PI / 180.

    let targetVelocity (state:GameStateSpec) = 
    
        let getTargetVelocity (trackPiece:TrackPiece) =

            let currentLane = trackPiece.Lanes.[state.CarPos.EndLane]

            let laneOptions = match currentLane.LeftLane,currentLane.RightLane with 
                              | None,None -> [ currentLane.Lane.Index ]
                              | Some left, None -> [ left; currentLane.Lane.Index ]
                              | None, Some right -> [ currentLane.Lane.Index; right ]
                              | Some left, Some right -> [ left; currentLane.Lane.Index; right ]

            let lookUpTargetVel id startLane endLane = 
                let key = { PId=id; StartLn=startLane; EndLn=endLane }
                match state.TargetVels.TryGetValue(key) with
                | true,v -> v.Velocity
                | _ ->
                    let lanes = state.Race.Track.Lanes
                    let laneOffset = lanes.[state.CarPos.EndLane].DistFromCentre
                
                    match trackPiece.Spec with 
                    | Bend b -> 
                        let trueR = if b.Angle > 0. then b.Radius - laneOffset else b.Radius + laneOffset
                        System.Math.Sqrt(trueR) * 0.65
                    | _ -> 30.0

            let targetVelocities = laneOptions |> List.map (fun laneId -> lookUpTargetVel trackPiece.Id state.CarPos.EndLane laneId)
            
            targetVelocities |> Seq.min

        let distNeededToSlowToTargetVelocity vInitial vTarget = 

            let rec distNeededToSlowToTargetVelocity' vCurrent vTarget distSoFar = 
                if vCurrent <= (max vTarget 1.0)
                then distSoFar + vCurrent
                else 
                    let vNext = vCurrent * state.Constants.Friction
                    distNeededToSlowToTargetVelocity' vNext vTarget (distSoFar + vCurrent)
            
            let distanceInNextTickIfWeDoFullThrottlNow = 
                (vInitial * state.Constants.Friction + 1.0 * state.Constants.Power)
            distNeededToSlowToTargetVelocity' vInitial vTarget distanceInNextTickIfWeDoFullThrottlNow

        let requiredVelocity vInitial (trackPiece, distToPiece) = 
            let vTarget = getTargetVelocity trackPiece
            let distNeeded = distNeededToSlowToTargetVelocity vInitial vTarget
            let distAvailable = max distToPiece 0.0
            if distNeeded > distAvailable then vTarget else 100.0

        let vInitial = state.Telemetry.Vel

        let minVelocity minSoFar trackPiece = 
            let requiredVelocity = requiredVelocity vInitial trackPiece 
            min minSoFar requiredVelocity

        let maxSafeVelocity = (nextPieces state) |> Seq.fold minVelocity 100.0

        maxSafeVelocity

    match state.GameTick with
    | x when x <= 3 -> 1.0 // to get power and friction
    | _ ->
        let emergencyResponseThrottleScale = 
            let angleTooGreat,ticks = isAngleGoingToGoOverMaxAngle log state
            let maxAngExceeded = (abs state.CarPos.Angle) > state.Constants.MaxAngle

            if maxAngExceeded then
                log <| sprintf "Emergency brake act Angl: angl=%f" (toDegrees (abs state.CarPos.Angle))
                0.0
            elif angleTooGreat then
                log <| sprintf "Emergency brake crash predicted in ticks=%d" ticks
                0.0
            else 
                1.0

        let throttle = (targetVelocity state - state.Telemetry.Vel * state.Constants.Friction) / state.Constants.Power |> min 1. |> max 0.
        throttle * emergencyResponseThrottleScale

let getMessageToSend log (state:GameStateSpec) =
    match useTurbo state with
    | true -> EngageTurbo
    | false -> 
        match getSwitchDirection log state with
        | Some (direction),pieceId -> Switch (direction,pieceId)
        | None,_ -> Throttle (getThrottle log state)


let basicAI log (GameState state) = 
    
    match state.MessageReceived, state.GameTick with
        | GameStart, _ -> getMessageToSend log state
        | CarPositions cps, t when t > 0 -> getMessageToSend log state
        | _ -> NoReply
