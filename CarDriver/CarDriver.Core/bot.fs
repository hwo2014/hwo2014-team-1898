﻿module bot

open System
open System.Threading

open GameLoop

[<EntryPoint>]
let main args =

    let startConsoleGame host port botName botKey = 
        let port = Int32.Parse(port)
        let joinMsg = SimpleJoin (botName,botKey)
        let doNothing = fun _ -> ()
        let log = fun gt msg -> printfn "%d: %s" gt msg

        let cts = new CancellationTokenSource()

        let game = new Game(host, port, doNothing, log, basicAI)
        game.start joinMsg true cts

    match args with 
    | [| host;port;botName;botKey |] -> startConsoleGame host port botName botKey
    | _ -> failwith "Invalid param array"
    
    0 //Have to return a value to the shell