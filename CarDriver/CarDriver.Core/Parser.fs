﻿module Parser

[<AutoOpen>]
module JsonParsing =

    open System.Collections.Generic
    open FSharp.Data
    open FSharp.Data.JsonExtensions 

    let getRecordProps = function
        | JsonValue.Record ps -> ps
        | x -> failwith ("Expected input to be JsonValue.Record but was: " + x.ToString())

    let getArrayVals = function
        | JsonValue.Array vs -> vs 
        | x -> failwith ("Expected input to be JsonValue.Array but was: " + x.ToString())

    let tryFind f name = 
       Array.tryFind (fun (k,v) -> k = name) 
       >> Option.map (fun (k,v) -> f v) 

    let findOrDefault f name def = 
       tryFind f name >> Option.fold (fun _ v -> v) def 

    let findOrFail f name ps =
        match ps |> Array.tryFind (fun (k,v) -> k = name) with
        | Some (k,v) -> f v
        | None -> failwith (sprintf "Unable to find property %A in %A" name ps)

    let findPropertiesOrFail f name ps =
        match ps |> Array.tryFind (fun (k,v) -> k = name) with
        | Some (k,v) -> f (getRecordProps v)
        | None -> failwith (sprintf "Unable to find property %A in %A" name ps)
   
    let parseInt = fun (v:JsonValue) -> v.AsInteger()
    let parseString = fun (v:JsonValue) -> v.AsString()
    let parseBool = fun (v:JsonValue) -> v.AsBoolean()
    let parseFloat = fun (v:JsonValue) -> v.AsFloat()
    
    let toRadians degrees = degrees * System.Math.PI /180.

    let parseCarId = 
        let cache = new Dictionary<_,_>()
        (fun ps -> 
            let name = ps |> findOrFail parseString "name"
            let colour = ps |> findOrDefault parseString "color" ""
            let key = name,colour
            match cache.TryGetValue(key) with
            | true,v -> v 
            | _ -> 
                let v = { Name = name; Colour = colour } 
                cache.Add(key,v)
                v
         )

    let parseLapTime ps = 
        match ps with 
        | [||] -> None
        | ps -> Some { Lap = ps |> findOrFail parseInt "lap"
                       LapTicks = ps |> findOrFail parseInt "ticks" 
                       LapMs = ps |> findOrFail parseInt "millis" }

    let parseRaceTime ps = 
        match ps with 
        | [||] -> None
        | ps -> Some { Laps = ps |> findOrDefault parseInt "laps" 0
                       RaceTicks = ps |> findOrFail parseInt "ticks" 
                       RaceMs = ps |> findOrFail parseInt "millis" }

let parseGameInit input = 

    let ps = getRecordProps input
    let ps = ps |> findOrFail getRecordProps "race"
 
    let parseTrack ps = 
        let parsePieces lanes input = 
            let parsePiece id ps = 
                let length = ps |> tryFind parseFloat "length"
                let radius = ps |> tryFind parseFloat "radius"
                let angle = ps |> tryFind (parseFloat >> toRadians) "angle"
                let isSwitch = ps |> findOrDefault parseBool "switch" false

                let leftSwitch isSwitch laneIndex = 
                    match isSwitch,laneIndex with 
                    | true, x when x > 0 -> Some (x-1) 
                    | _ -> None
                
                let rightSwitch isSwitch laneIndex maxLane = 
                    match isSwitch,laneIndex with 
                    | true, x when x < maxLane -> Some (x+1) 
                    | _ -> None

                let trackLanes = lanes |> Map.toArray |> Array.map (fun (id,lane) -> 
                    let laneLength = match length, radius, angle with 
                                     | Some length, None, None -> length
                                     | None, Some radius, Some angle -> 
                                        let offset = lane.DistFromCentre
                                        let radius = radius + if angle < 0. then offset else -offset
                                        abs(angle) * radius
                                     | _, _, _ -> 0.
                    { Lane = lane; 
                      Length = laneLength; 
                      LeftLane=leftSwitch isSwitch lane.Index; 
                      RightLane=rightSwitch isSwitch lane.Index (lanes.Count-1) }) 
                
                let spec =       
                    match length, radius, angle with 
                    | Some length, None, None -> Straight {Length = length}
                    | None, Some radius, Some angle -> Bend {Angle=angle; Radius=radius}
                    | _, _, _ -> Straight {Length = 100.0} // This shouldn't happen, but is this better than an exception?
                
                { Id=id; Spec=spec; IsSwitch=isSwitch; Lanes=trackLanes }

            input
            |> getArrayVals
            |> Array.map getRecordProps
            |> Array.mapi parsePiece
            |> Array.toList
            
        let parseLanes input =
            let parseLane ps =
                { Index = ps |> findOrDefault parseInt "index" 0 
                  DistFromCentre = ps |> findOrDefault parseFloat "distanceFromCenter" 0.0 }

            input
            |> getArrayVals 
            |> Array.map getRecordProps
            |> Array.map parseLane
            |> Array.map (fun l -> l.Index, l)
            |> Map.ofArray

        let parseStartingPoint input =
            
            let parsePoint data =
                let ps = getRecordProps data
                ps |> findOrDefault parseFloat "x" 650. ,
                ps |> findOrDefault parseFloat "y" 300.

            let ps = getRecordProps input
            let point = ps |> tryFind parsePoint "position"
            let angle = ps |> tryFind parseFloat "angle"
            match point,angle with
            | Some (x,y), Some a -> { X=x; Y=y; A=toRadians (a-90.) }
            | _ -> { X = 650.; Y = 300.; A = 0. }

        let lanes = ps |> findOrFail parseLanes "lanes" 
        let pieces = ps |> findOrFail (parsePieces lanes) "pieces"; 

        { Track.Id = ps |> findOrDefault parseString "id" "unknown";
          Name = ps |> findOrDefault parseString "name" "Unknown"; 
          Lanes = lanes
          Pieces = pieces
          Origin = ps |> findOrDefault parseStartingPoint "startingPoint" { X = 650.; Y = 300.; A = 0. } }
 
    let parseCars input = 
        let parseCar ps = 
            let dimensions = ps |> findOrFail getRecordProps "dimensions" 

            { Car.Id = ps |> findPropertiesOrFail parseCarId "id"
              Width = dimensions |> findOrFail parseFloat "width" 
              Length = dimensions |> findOrFail parseFloat "length"
              GuideFlagPos = dimensions |> findOrFail parseFloat "guideFlagPosition" }

        input
        |> getArrayVals
        |> Array.map getRecordProps
        |> Array.map parseCar
        |> Array.map (fun car -> car.Id, car)
        |> Map.ofArray
        

    let parseSession ps = 
        let quickRace = ps |> findOrDefault parseBool "quickRace" false
        let lapCount = ps |> tryFind parseInt "laps"
        let maxLapTimeMs = ps |> tryFind parseInt "maxLapTimeMs"
        let durationMs = ps |> tryFind parseInt "durationMs"
        
        match quickRace,lapCount,maxLapTimeMs,durationMs with
        | true, Some lc, Some mlt, _ -> QuickRace { LapCount = lc; MaxLapTimeMs = mlt }
        | false, Some lc, Some mlt, _ -> FullRace { LapCount = lc; MaxLapTimeMs = mlt }
        | false, _, _, Some durationMs -> Qualifying durationMs
        | _ -> QuickRace { LapCount =  0; MaxLapTimeMs = 60000; }

    
    { Track = ps |> findPropertiesOrFail parseTrack "track"
      Cars = ps |> findOrFail parseCars "cars" 
      Session = ps |> findPropertiesOrFail parseSession "raceSession" }


let parseCarPositions ps =

    let parseCarPosition ps =
        let positionProps = ps |> findPropertiesOrFail id "piecePosition"
        let laneProps = positionProps |> findPropertiesOrFail id "lane"
    
        { CarId = ps |> findPropertiesOrFail parseCarId "id"
          Angle = ps |> findOrFail (parseFloat >> toRadians) "angle"
          Lap = positionProps |> findOrFail parseInt "lap"
          PieceId = positionProps |> findOrFail parseInt "pieceIndex"
          DistInPiece = positionProps |> findOrFail parseFloat "inPieceDistance"
          StartLane = laneProps |> findOrFail parseInt "startLaneIndex"
          EndLane = laneProps |> findOrFail parseInt "endLaneIndex" }

    ps 
    |> getArrayVals
    |> Array.map getRecordProps
    |> Array.map parseCarPosition

let parseYourCar data = data |> getRecordProps |> parseCarId |> YourCar

let parseCrash data = data |> getRecordProps |> parseCarId |> Crash

let parseSpawn data = data |> getRecordProps |> parseCarId |> Spawn

let parseDnf data = 
    let ps = data |> getRecordProps
    
    let car = ps |> findPropertiesOrFail parseCarId "car"
    let reason = ps |> findOrDefault parseString "reason" "Unknown"
    Dnf (car, reason)

let parseTurboAvailable data = 
    let ps = data |> getRecordProps
    TurboAvailable { Factor = ps |> findOrFail parseFloat "turboFactor"
                     Ticks = ps |> findOrFail parseInt "turboDurationTicks"
                     Ms = ps |> findOrFail parseInt "turboDurationMilliseconds" }

let parseTurboStart data = data |> getRecordProps |> parseCarId |> TurboStart

let parseTurboEnd data = data |> getRecordProps |> parseCarId |> TurboEnd

let parseFinish data = data |> getRecordProps |> parseCarId |> Finish

let parseLapFinished data =
    let ps = data |> getRecordProps

    let carId = ps |> findPropertiesOrFail parseCarId "car"
    let lapTime = ps |> findPropertiesOrFail parseLapTime "lapTime"
    let raceTime = ps |> findPropertiesOrFail parseRaceTime "raceTime"
        
    match lapTime,raceTime with 
    | Some lt, Some rt -> LapFinished { CarId = carId; Time = lt; RaceTime = rt }
    | None, _ -> failwith "Unable to parse lap time"
    | _, None -> failwith "Unable to parse race time"

let parseGameFinished data =

    let parseResult ps = 
        let car = ps |> findPropertiesOrFail parseCarId "car"
        let raceTime = ps |> findPropertiesOrFail parseRaceTime "result"
        car,raceTime
        
    let parseBestLap ps = 
        let car = ps |> findPropertiesOrFail parseCarId "car"
        let lapTime = ps |> findPropertiesOrFail parseLapTime "result"
        car,lapTime

    let ps = data |> getRecordProps
    let results = ps |> findOrFail getArrayVals "results" |> Array.map getRecordProps |> Array.map parseResult
    let bestLaps = ps |> findOrFail getArrayVals "bestLaps" |> Array.map getRecordProps |> Array.map parseBestLap
    
    results 
    |> Array.map (fun (c,r) -> 
        let bestLap = bestLaps |> Array.tryFind (fun (c2,l) -> c = c2) |> Option.fold (fun s (c2,l) -> l) None
        { CarId = c; RaceTime = r; BestLap = bestLap }
    )
    |> Array.toList
    |> GameEnd


