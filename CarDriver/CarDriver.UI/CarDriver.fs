﻿open System
open System.Drawing
open System.Threading
open System.Windows.Forms
open System.IO

open Domain
open Parser
open GameLoop
open Visualisation
open MainForm
open UiUpdater

[<EntryPoint>]
let main args =

    let startConsoleGame host port botName botKey = 
        let port = Int32.Parse(port)
        let joinMsg = SimpleJoin (botName,botKey)
        let doNothing = fun _ -> ()
        let log = fun gt msg -> printfn "%d: %s" gt msg

        let game = new Game(host, port, doNothing, log, basicAI)
        let cts = new CancellationTokenSource()
        game.start joinMsg true cts

    let startUI host port botName botKey = 
        let visualisationForm = new VisualisationForm()
        let logPath = (String.Format("c:\\temp\{0}-{1:yyMMdd-HHmmss}.csv", botName, DateTime.Now))
        let logger = new StreamWriter(logPath)
        let log gt m =
            visualisationForm.AppendLogMessage gt m
            logger.WriteLine( sprintf "%d: %s" gt m )

        let mainForm = new MainForm (host,port,botName,botKey,log)
        let uiUpdater = new UiUpdater (mainForm, visualisationForm)
        mainForm.UpdateAction <- uiUpdater.update
        visualisationForm.SetCloseAction mainForm.CancelRace

        do Application.Run(mainForm); logger.Flush()

    match args with 
    | [| host;port;botName;botKey |] -> startConsoleGame host port botName botKey
    | [| host;port;botName;botKey;"showUI" |] -> startUI host port botName botKey 
    | [| |] -> startUI "" "" "" ""
    | _ -> failwith "Invalid param array"
    
    0 //Have to return a value to the shell
