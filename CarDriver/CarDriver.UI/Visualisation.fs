﻿module Visualisation

open System
open System.Collections.Generic
open System.Drawing
open System.Windows.Forms
open FSharp.Charting

open MainForm

type DrawStraight = { StartPoint:Origin; EndPoint:Origin; Length:float; IsSwitch:bool }
type DrawBend = { StartPoint:Origin; EndPoint:Origin; Length:float; Centre:Origin; Radius:float; Arc:float; IsSwitch:bool }

type DrawPiece = | DrawStraight of DrawStraight
                 | DrawBend of DrawBend

type VisualisationForm() as this =
    inherit Form()

    let mutable closeAction = fun () -> ()
    let mutable drawPieces = []
    let mutable lanes = []
    let mutable background = null
    let mutable invalidatedRegion = new Rectangle (0,0,1,1)
    let mutable chartDisplayed = false

    let targetFps = 20
    
    let LanePen = new Pen(Color.Gray, 3.f)
    let TrackPen = new Pen(Color.DodgerBlue, 2.f)
    let CarPen = new Pen(Color.ForestGreen, 5.f)
    let TurboPen = new Pen(Color.Yellow, 5.f)

    let grpCarData = new GroupBox()
    let lblSwitch = new Label()
    let txtLane = new TextBox()
    let lblThrottle = new Label()
    let txtThrottle = new TextBox()
    let lblPiece = new Label()
    let txtPiece = new TextBox()
    let lblDistInPiece = new Label()
    let txtDistInPiece = new TextBox()
    let lblAngle = new Label()
    let txtAngle = new TextBox()
    let lblVel = new Label()
    let txtVel = new TextBox()
    let lblCarLap = new Label()
    let txtCarLap = new TextBox()
    let grpRaceData = new GroupBox()
    let lblStage = new Label()
    let txtStage = new TextBox()
    let lblTrack = new Label()
    let txtTrack = new TextBox()
    let lblOf = new Label()
    let txtTotalLaps = new TextBox()
    let lblRaceLap = new Label()
    let txtRaceLap = new TextBox()
    let lblGameTick = new Label()
    let txtGameTick = new TextBox()
    let pnlTop = new Panel()
    let splitter = new SplitContainer()
    let picTrack = new PictureBox()
    let txtLog = new TextBox()
    let lblTurbo = new Label()
    let txtTurbo = new TextBox()
    let lblAcc = new Label()
    let txtAcc = new TextBox()
    let lblAngAcc = new Label()
    let txtAngAcc = new TextBox()
    let lblAngVel = new Label()
    let txtAngVel = new TextBox()
    
    do
        grpCarData.SuspendLayout()
        grpRaceData.SuspendLayout()
        pnlTop.SuspendLayout()
        //((System.ComponentModel.ISupportInitialize)(this.splitter)).BeginInit()
        splitter.Panel1.SuspendLayout()
        splitter.Panel2.SuspendLayout()
        splitter.SuspendLayout()
        // ((System.ComponentModel.ISupportInitialize)(this.picTrack)).BeginInit()
        this.SuspendLayout()

        // 
        // grpCarData
        // 
        grpCarData.Controls.Add(lblAngAcc)
        grpCarData.Controls.Add(txtAngAcc)
        grpCarData.Controls.Add(lblAngVel)
        grpCarData.Controls.Add(txtAngVel)
        grpCarData.Controls.Add(lblAcc)
        grpCarData.Controls.Add(txtAcc)
        grpCarData.Controls.Add(lblTurbo)
        grpCarData.Controls.Add(txtTurbo)
        grpCarData.Controls.Add(lblSwitch)
        grpCarData.Controls.Add(txtLane)
        grpCarData.Controls.Add(lblThrottle)
        grpCarData.Controls.Add(txtThrottle)
        grpCarData.Controls.Add(lblPiece)
        grpCarData.Controls.Add(txtPiece)
        grpCarData.Controls.Add(lblDistInPiece)
        grpCarData.Controls.Add(txtDistInPiece)
        grpCarData.Controls.Add(lblAngle)
        grpCarData.Controls.Add(txtAngle)
        grpCarData.Controls.Add(lblVel)
        grpCarData.Controls.Add(txtVel)
        grpCarData.Controls.Add(lblCarLap)
        grpCarData.Controls.Add(txtCarLap)
        grpCarData.Dock <- DockStyle.Fill
        grpCarData.Location <- new Point(223, 0)
        grpCarData.Name <- "grpCarData"
        grpCarData.Size <- new Size(666, 123)
        grpCarData.TabIndex <- 3
        grpCarData.TabStop <- false
        grpCarData.Text <- "Car Data"
        // 
        // lblAngAcc
        // 
        lblAngAcc.AutoSize <- true
        lblAngAcc.Location <- new Point(562, 19)
        lblAngAcc.Name <- "lblAngAcc"
        lblAngAcc.Size <- new Size(48, 13)
        lblAngAcc.TabIndex <- 21
        lblAngAcc.Text <- "Ang Acc"
        // 
        // txtAngAcc
        // 
        txtAngAcc.Location <- new Point(561, 36)
        txtAngAcc.Name <- "txtAngAcc"
        txtAngAcc.Size <- new Size(47, 20)
        txtAngAcc.TabIndex <- 20
        txtAngAcc.TextAlign <- HorizontalAlignment.Right
        // 
        // lblAngVel
        // 
        lblAngVel.AutoSize <- true
        lblAngVel.Location <- new Point(512, 19)
        lblAngVel.Name <- "lblAngVel"
        lblAngVel.Size <- new Size(44, 13)
        lblAngVel.TabIndex <- 19
        lblAngVel.Text <- "Ang Vel"
        // 
        // txtAngVel
        // 
        txtAngVel.Location <- new Point(511, 36)
        txtAngVel.Name <- "txtAngVel"
        txtAngVel.Size <- new Size(47, 20)
        txtAngVel.TabIndex <- 18
        txtAngVel.TextAlign <- HorizontalAlignment.Right
        // 
        // lblAcc
        // 
        lblAcc.AutoSize <- true
        lblAcc.Location <- new Point(460, 19)
        lblAcc.Name <- "lblAcc"
        lblAcc.Size <- new Size(26, 13)
        lblAcc.TabIndex <- 17
        lblAcc.Text <- "Acc"
        // 
        // txtAcc
        // 
        txtAcc.Location <- new Point(461, 36)
        txtAcc.Name <- "txtAcc"
        txtAcc.Size <- new Size(47, 20)
        txtAcc.TabIndex <- 16
        txtAcc.TextAlign <- HorizontalAlignment.Right
        // 
        // lblTurbo
        // 
        lblTurbo.AutoSize <- true
        lblTurbo.Location <- new Point(261, 19)
        lblTurbo.Name <- "lblTurbo"
        lblTurbo.Size <- new Size(35, 13)
        lblTurbo.TabIndex <- 15
        lblTurbo.Text <- "Turbo"
        // 
        // txtTurbo
        // 
        txtTurbo.Location <- new Point(262, 36)
        txtTurbo.Name <- "txtTurbo"
        txtTurbo.Size <- new Size(134, 20)
        txtTurbo.TabIndex <- 14
        txtTurbo.TextAlign <- HorizontalAlignment.Left
        // 
        // lblSwitch
        // 
        lblSwitch.AutoSize <- true
        lblSwitch.Location <- new Point(216, 19)
        lblSwitch.Name <- "lblSwitch"
        lblSwitch.Size <- new Size(31, 13)
        lblSwitch.TabIndex <- 13
        lblSwitch.Text <- "Lane"
        // 
        // txtLane
        // 
        txtLane.Location <- new Point(213, 36)
        txtLane.Name <- "txtLane"
        txtLane.Size <- new Size(46, 20)
        txtLane.TabIndex <- 12
        txtLane.TextAlign <- HorizontalAlignment.Right
        // 
        // lblThrottle
        // 
        lblThrottle.AutoSize <- true
        lblThrottle.Location <- new Point(167, 19)
        lblThrottle.Name <- "lblThrottle"
        lblThrottle.Size <- new Size(43, 13)
        lblThrottle.TabIndex <- 11
        lblThrottle.Text <- "Throttle"
        // 
        // txtThrottle
        // 
        txtThrottle.Location <- new Point(168, 36)
        txtThrottle.Name <- "txtThrottle"
        txtThrottle.Size <- new Size(42, 20)
        txtThrottle.TabIndex <- 10
        txtThrottle.TextAlign <- HorizontalAlignment.Right
        // 
        // lblPiece
        // 
        lblPiece.AutoSize <- true
        lblPiece.Location <- new Point(33, 19)
        lblPiece.Name <- "lblPiece"
        lblPiece.Size <- new Size(34, 13)
        lblPiece.TabIndex <- 9
        lblPiece.Text <- "Piece"
        // 
        // txtPiece
        // 
        txtPiece.Location <- new Point(34, 36)
        txtPiece.Name <- "txtPiece"
        txtPiece.Size <- new Size(33, 20)
        txtPiece.TabIndex <- 8
        txtPiece.TextAlign <- HorizontalAlignment.Right
        // 
        // lblDistInPiece
        // 
        lblDistInPiece.AutoSize <- true
        lblDistInPiece.Location <- new Point(69, 19)
        lblDistInPiece.Name <- "lblDistInPiece"
        lblDistInPiece.Size <- new Size(25, 13)
        lblDistInPiece.TabIndex <- 7
        lblDistInPiece.Text <- "Dist"
        // 
        // txtDistInPiece
        // 
        txtDistInPiece.Location <- new Point(70, 36)
        txtDistInPiece.Name <- "txtDistInPiece"
        txtDistInPiece.Size <- new Size(43, 20)
        txtDistInPiece.TabIndex <- 6
        txtDistInPiece.TextAlign <- HorizontalAlignment.Right
        // 
        // lblAngle
        // 
        lblAngle.AutoSize <- true
        lblAngle.Location <- new Point(115, 19)
        lblAngle.Name <- "lblAngle"
        lblAngle.Size <- new Size(34, 13)
        lblAngle.TabIndex <- 5
        lblAngle.Text <- "Angle"
        // 
        // txtAngle
        // 
        txtAngle.Location <- new Point(116, 36)
        txtAngle.Name <- "txtAngle"
        txtAngle.Size <- new Size(36, 20)
        txtAngle.TabIndex <- 4
        txtAngle.TextAlign <- HorizontalAlignment.Right
        // 
        // lblVel
        // 
        lblVel.AutoSize <- true
        lblVel.Location <- new Point(410, 19)
        lblVel.Name <- "lblVel"
        lblVel.Size <- new Size(22, 13)
        lblVel.TabIndex <- 3
        lblVel.Text <- "Vel"
        // 
        // txtVel
        // 
        txtVel.Location <- new Point(411, 36)
        txtVel.Name <- "txtVel"
        txtVel.Size <- new Size(47, 20)
        txtVel.TabIndex <- 2
        txtVel.TextAlign <- HorizontalAlignment.Right
        // 
        // lblCarLap
        // 
        lblCarLap.AutoSize <- true
        lblCarLap.Location <- new Point(5, 19)
        lblCarLap.Name <- "lblCarLap"
        lblCarLap.Size <- new Size(25, 13)
        lblCarLap.TabIndex <- 1
        lblCarLap.Text <- "Lap"
        // 
        // txtCarLap
        // 
        txtCarLap.Location <- new Point(6, 36)
        txtCarLap.Name <- "txtCarLap"
        txtCarLap.Size <- new Size(25, 20)
        txtCarLap.TabIndex <- 0
        txtCarLap.TextAlign <- HorizontalAlignment.Right
        // 
        // grpRaceData
        // 
        grpRaceData.Controls.Add(lblStage)
        grpRaceData.Controls.Add(txtStage)
        grpRaceData.Controls.Add(lblTrack)
        grpRaceData.Controls.Add(txtTrack)
        grpRaceData.Controls.Add(lblOf)
        grpRaceData.Controls.Add(txtTotalLaps)
        grpRaceData.Controls.Add(lblRaceLap)
        grpRaceData.Controls.Add(txtRaceLap)
        grpRaceData.Controls.Add(lblGameTick)
        grpRaceData.Controls.Add(txtGameTick)
        grpRaceData.Dock <- DockStyle.Left
        grpRaceData.Location <- new Point(0, 0)
        grpRaceData.Name <- "grpRaceData"
        grpRaceData.Size <- new Size(223, 123)
        grpRaceData.TabIndex <- 0
        grpRaceData.TabStop <- false
        grpRaceData.Text <- "Race Data"
        // 
        // txtGameTick
        // 
        txtGameTick.Location <- new Point(48, 95)
        txtGameTick.Name <- "txtGameTick"
        txtGameTick.Size <- new Size(78, 20)
        txtGameTick.TabIndex <- 0
        txtGameTick.TextAlign <- HorizontalAlignment.Right
        // 
        // lblGameTick
        // 
        lblGameTick.AutoSize <- true
        lblGameTick.Location <- new Point(7, 98)
        lblGameTick.Name <- "lblGameTick"
        lblGameTick.Size <- new Size(28, 13)
        lblGameTick.TabIndex <- 1
        lblGameTick.Text <- "Tick"
        // 
        // pnlTop
        // 
        pnlTop.Controls.Add(grpCarData)
        pnlTop.Controls.Add(grpRaceData)
        pnlTop.Dock <- DockStyle.Top
        pnlTop.Location <- new Point(0, 0)
        pnlTop.Name <- "pnlTop"
        pnlTop.Size <- new Size(889, 123)
        pnlTop.TabIndex <- 4
        // 
        // splitter
        // 
        splitter.Dock <- DockStyle.Fill
        splitter.Location <- new Point(0, 123)
        splitter.Name <- "splitter"
        splitter.Orientation <- Orientation.Horizontal
        // 
        // splitter.Panel1
        // 
        splitter.Panel1.Controls.Add(picTrack)
        // 
        // splitter.Panel2
        // 
        splitter.Panel2.Controls.Add(txtLog)
        splitter.Size <- new Size(889, 436)
        splitter.SplitterDistance <- 295
        splitter.TabIndex <- 6
        // 
        // txtLog
        // 
        txtLog.Dock <- DockStyle.Fill
        txtLog.Location <- new Point(0, 0)
        txtLog.Multiline <- true
        txtLog.Name <- "txtLog"
        txtLog.ScrollBars <- ScrollBars.Both
        txtLog.Size <- new Size(889, 137)
        txtLog.TabIndex <- 2
        txtLog.Text <- ""
        // 
        // picTrack
        // 
        picTrack.Dock <- DockStyle.Fill
        picTrack.Location <- new Point(0, 0)
        picTrack.Name <- "picTrack"
        picTrack.Size <- new Size(889, 295)
        picTrack.TabIndex <- 6
        picTrack.TabStop <- false
        picTrack.BackColor <- Color.Black
        picTrack.SizeMode <- PictureBoxSizeMode.Zoom
        // 
        // lblRaceLap
        // 
        lblRaceLap.AutoSize <- true
        lblRaceLap.Location <- new Point(7, 72)
        lblRaceLap.Name <- "lblRaceLap"
        lblRaceLap.Size <- new Size(25, 13)
        lblRaceLap.TabIndex <- 3
        lblRaceLap.Text <- "Lap"
        // 
        // txtRaceLap
        // 
        txtRaceLap.Location <- new Point(48, 69)
        txtRaceLap.Name <- "txtRaceLap"
        txtRaceLap.Size <- new Size(25, 20)
        txtRaceLap.TabIndex <- 2
        txtRaceLap.TextAlign <- HorizontalAlignment.Right
        // 
        // lblOf
        // 
        lblOf.AutoSize <- true
        lblOf.Location <- new Point(79, 72)
        lblOf.Name <- "lblOf"
        lblOf.Size <- new Size(16, 13)
        lblOf.TabIndex <- 5
        lblOf.Text <- "of"
        // 
        // txtTotalLaps
        // 
        txtTotalLaps.Location <- new Point(101, 69)
        txtTotalLaps.Name <- "txtTotalLaps"
        txtTotalLaps.Size <- new Size(25, 20)
        txtTotalLaps.TabIndex <- 4
        txtTotalLaps.TextAlign <- HorizontalAlignment.Right
        // 
        // lblTrack
        // 
        lblTrack.AutoSize <- true
        lblTrack.Location <- new Point(7, 20)
        lblTrack.Name <- "lblTrack"
        lblTrack.Size <- new Size(35, 13)
        lblTrack.TabIndex <- 7
        lblTrack.Text <- "Track"
        // 
        // txtTrack
        // 
        txtTrack.Location <- new Point(48, 17)
        txtTrack.Name <- "txtTrack"
        txtTrack.Size <- new Size(169, 20)
        txtTrack.TabIndex <- 6
        txtTrack.TextAlign <- HorizontalAlignment.Left
        // 
        // lblStage
        // 
        lblStage.AutoSize <- true
        lblStage.Location <- new Point(7, 46)
        lblStage.Name <- "lblStage"
        lblStage.Size <- new Size(35, 13)
        lblStage.TabIndex <- 9
        lblStage.Text <- "Stage"
        // 
        // txtStage
        // 
        txtStage.Location <- new Point(48, 43)
        txtStage.Name <- "txtStage"
        txtStage.Size <- new Size(169, 20)
        txtStage.TabIndex <- 8
        txtStage.TextAlign <- HorizontalAlignment.Left
        // 
        // Form1
        // 
        this.AutoScaleDimensions <- new SizeF(6.f, 13.f)
        this.AutoScaleMode <- AutoScaleMode.Font
        
        this.ClientSize <- new Size(1024, 768)
        this.Controls.Add(splitter)
        this.Controls.Add(pnlTop)
        this.Name <- "Race Visualisation"
        this.Text <- "Race Visualisation"
        grpCarData.ResumeLayout(false)
        grpCarData.PerformLayout()
        grpRaceData.ResumeLayout(false)
        grpRaceData.PerformLayout()
        pnlTop.ResumeLayout(false)
        splitter.Panel1.ResumeLayout(false)
        splitter.Panel2.ResumeLayout(false)
        splitter.Panel2.PerformLayout()
        //((System.ComponentModel.ISupportInitialize)(this.splitter)).EndInit()
        splitter.ResumeLayout(false)
        //((System.ComponentModel.ISupportInitialize)(this.picTrack)).EndInit()
        this.ResumeLayout(false)

    let invokeOnUiThread action = 
            this.Invoke(new MethodInvoker(action)) |> ignore

    let logStringBuilder = new System.Text.StringBuilder()

    let updateLogUI () =

        if this.Visible then
            this.Invoke(new MethodInvoker(fun _ -> 
                    txtLog.AppendText(logStringBuilder.ToString())
                    logStringBuilder.Clear() |> ignore
                    )) |> ignore
    let updateCarPosition (carPosition:CarPosition) = 
        
        let rightAngle = Math.PI / 2.0
        let drawCar (graphics:Graphics) carPos =
            
            let piece = drawPieces.[carPos.PieceId]
            let dist = carPos.DistInPiece
            let lane1Offset = lanes.[carPos.StartLane].DistFromCentre
            let lane2Offset = lanes.[carPos.EndLane].DistFromCentre
                        
            let x,y = 
                match piece with 
                | DrawStraight s -> 
                    let ratio = dist / s.Length
                    let x1,y1 = s.StartPoint.X, s.StartPoint.Y
                    let x2,y2 = s.EndPoint.X, s.EndPoint.Y
                    let cx = x1 + (ratio * (x2 - x1))
                    let cy = y1 + (ratio * (y2 - y1))
                    let angle = s.StartPoint.A
                    let offset = lane1Offset + (ratio * (lane2Offset - lane1Offset))
                    let x = cx + Math.Cos(angle + rightAngle) * offset
                    let y = cy + Math.Sin(angle + rightAngle) * offset
                    x,y

                | DrawBend b -> 
                    let ratio = dist / b.Length
                    let x,y = b.Centre.X, b.Centre.Y
                    let a1 = b.Centre.A
                    let a2 = a1 + b.Arc
                    let offset = lane1Offset + (ratio * (lane2Offset - lane1Offset))
                    let radius = if (a1 > a2) then b.Radius + offset else b.Radius - offset
                    let angle = a1 + (ratio * (a2-a1))
                    x + (cos (angle) * radius), y + (sin (angle) * radius)
                     
            graphics.DrawImage(background, invalidatedRegion, invalidatedRegion, GraphicsUnit.Pixel);
            graphics.DrawEllipse(CarPen, float32 (x-5.), float32 (y-5.), 10.f, 10.f)
            invalidatedRegion <- new Rectangle (int x-10, int y-10, 20, 20)

        let graphics = Graphics.FromImage(picTrack.Image)
        drawCar graphics carPosition
        picTrack.Invalidate()

        txtCarLap.Text <- carPosition.Lap.ToString()
        txtRaceLap.Text <- carPosition.Lap.ToString()
        txtPiece.Text <- carPosition.PieceId.ToString()
        txtDistInPiece.Text <- carPosition.DistInPiece.ToString("0.00")
        txtAngle.Text <- (carPosition.Angle * 180. / Math.PI).ToString("0.00")

        txtLane.Text <- 
            match carPosition.StartLane,carPosition.EndLane with
            | lane1,lane2 when lane1 < lane2 -> sprintf "%d --> %d" lane1 lane2
            | lane1,lane2 when lane1 > lane2 -> sprintf "%d <-- %d" lane1 lane2
            | lane1,lane2 -> lane1.ToString()


    let chartTimer = new System.Windows.Forms.Timer(Interval=1000/targetFps, Enabled=true)
    let gameStateSample = ref (match GameState.Default with GameState x -> x)
    let targetVelocityChartEvent = new Event<_>()
    let targetVelocityChart = 
        LiveChart.Column(targetVelocityChartEvent.Publish,Name="Target Velocity")
            .WithYAxis(Min=0.0, Max=10.0)

    let velocityChartEvent = new Event<_>()
    let velocityChart = 
        LiveChart.FastLine(velocityChartEvent.Publish,Name="Velocity")
            .WithXAxis( Min=0.0)
            .WithYAxis(Min=0.0, Max=20.0)

    let throttleChartEvent = new Event<_>()
    let throttleChart = 
        LiveChart.FastLine(throttleChartEvent.Publish,Name="Throttle")
            .WithYAxis( Min=0.0, Max=1.)
    
    let angleChartEvent = new Event<_>()
    let AngleChart = 
        LiveChart.FastLine(angleChartEvent.Publish,Name="Angle")
            .WithXAxis(Min=0.0)
            .WithYAxis( Min=(-60.0), Max=60.0)

    let charts = [ targetVelocityChart; velocityChart; throttleChart; AngleChart ]
    let combinedChart = Chart.Rows charts

    override this.OnVisibleChanged e = 
        base.OnVisibleChanged e
        txtLog.Text <- ""
        updateLogUI()

    override this.OnClosing e =
        e.Cancel <- true
        closeAction()
        this.Hide()

    member this.setRace race = 

        let rightAngle = Math.PI / 2.0
        
        let drawLine (graphics:Graphics) pen x1 y1 x2 y2 = graphics.DrawLine(pen, float32 x1, float32 y1, float32 x2, float32 y2)
        let drawArc (graphics:Graphics) pen left top length startAngle sweepAngle = graphics.DrawArc(pen, float32 left, float32 top, float32 length, float32 length, float32 (startAngle / Math.PI * 180.), float32 (sweepAngle / Math.PI * 180.))
        
        let growBoundingBox (box:Rectangle) drawPiece =
            let x,y = match drawPiece with 
                      | DrawStraight s -> s.StartPoint.X,s.StartPoint.Y
                      | DrawBend b -> b.StartPoint.X,b.StartPoint.Y
                      
            let positive x = if x > 0 then x else 0 
            let px = int x
            let py = int y
            let growLeft = positive (box.X-px+50)
            let growTop = positive (box.Y-py+50)
            let growRight = positive (px - box.X - box.Width + 50)
            let growBottom = positive (py - box.Y - box.Height + 50)

            new Rectangle (box.X - growLeft, box.Y - growTop, box.Width + growLeft + growRight, box.Height + growTop + growBottom)

        let drawStraightLane graphics (d:DrawStraight) pen offset = 
            let angle = d.StartPoint.A
            let startX = d.StartPoint.X + Math.Cos(angle + rightAngle) * offset
            let startY = d.StartPoint.Y + Math.Sin(angle + rightAngle) * offset

            let endX = d.EndPoint.X + Math.Cos(angle + rightAngle) * offset
            let endY = d.EndPoint.Y + Math.Sin(angle + rightAngle) * offset

            drawLine graphics pen startX startY endX endY

        let drawBendLane graphics (b:DrawBend) pen offset =
            let angle = b.Centre.A
            let radius = b.Radius + offset

            drawArc graphics pen (b.Centre.X - radius) (b.Centre.Y - radius) (radius * 2.) angle b.Arc

        let calculateNextPoint (_,previousPiece) currentPiece = 

            let startPoint = match previousPiece with 
                             | DrawStraight s -> s.EndPoint
                             | DrawBend d -> d.EndPoint

            let startAngle = startPoint.A
                        
            match currentPiece.Spec with
            | Straight s ->
                let endX = startPoint.X + Math.Cos(startAngle) * s.Length
                let endY = startPoint.Y + Math.Sin(startAngle) * s.Length
                let endPoint = { X=endX; Y=endY; A=startPoint.A }
                previousPiece, DrawStraight ({ StartPoint=startPoint; EndPoint = endPoint; Length = s.Length; IsSwitch = currentPiece.IsSwitch })
    
            | Bend b ->
       
                let flip = if b.Angle < 0. then -1. else 1.
                
                let centreAngle = startAngle - (rightAngle * flip)
                let centreX = startPoint.X - Math.Sin(startAngle) * b.Radius * flip
                let centreY = startPoint.Y + Math.Cos(startAngle) * b.Radius * flip
                let centre = { X=centreX; Y=centreY; A=centreAngle } 
                
                let endAngle = startAngle + b.Angle
                let endX = centreX + Math.Sin(endAngle) * b.Radius * flip
                let endY = centreY - Math.Cos(endAngle) * b.Radius * flip
                let endPoint = { X=endX; Y=endY; A=endAngle }

                let length = abs (b.Radius * b.Angle)

                previousPiece, DrawBend ({ StartPoint=startPoint; EndPoint = endPoint; Length = length; Centre=centre; Radius = b.Radius; Arc=b.Angle; IsSwitch = currentPiece.IsSwitch })

        let drawSwitch graphics lanes piece = 
            
            let drawSwitch off1 off2 =
                let x1,y1,x2,y2,a1,a2 = match piece with 
                                        | DrawStraight s -> s.StartPoint.X, s.StartPoint.Y, s.EndPoint.X, s.EndPoint.Y, s.StartPoint.A, s.EndPoint.A
                                        | DrawBend b -> b.StartPoint.X, b.StartPoint.Y, b.EndPoint.X, b.EndPoint.Y, b.StartPoint.A, b.EndPoint.A

                let startX = x1 + Math.Sin(a1) * off1
                let startY = y1 + Math.Cos(a2) * off2

                let endX = x2 + Math.Sin(a2) * off2
                let endY = y2 + Math.Cos(a1) * off1

                drawLine graphics LanePen startX startY endX endY

            let laneOffsets = lanes |> Seq.map (fun l -> l.DistFromCentre)
            laneOffsets |> Seq.iter2 (fun a b -> drawSwitch a b;drawSwitch b a) (laneOffsets |> Seq.skip 1)


        let drawPiece graphics lanes piece = 
            
            let insideEdge = lanes |> List.map (fun x -> x.DistFromCentre - 20.) |> List.min
            let outsideEdge = lanes |> List.map (fun x -> x.DistFromCentre + 20.) |> List.max
            let drawLanes = lanes |> List.map (fun x -> x.DistFromCentre)

            match piece with
            | DrawStraight d -> 
                drawStraightLane graphics d TrackPen insideEdge
                drawStraightLane graphics d TrackPen outsideEdge
                drawLanes |> List.iter (drawStraightLane graphics d LanePen)
                if d.IsSwitch then drawSwitch graphics lanes piece

            | DrawBend b ->  
                drawBendLane graphics b TrackPen insideEdge
                drawBendLane graphics b TrackPen outsideEdge
                drawLanes |> List.iter (drawBendLane graphics b LanePen)
                if b.IsSwitch then drawSwitch graphics lanes piece

        let generateImage race = 

            let pieces = race.Track.Pieces
            
            let origin = race.Track.Origin
            let seedPiece = DrawStraight ({ StartPoint=origin; EndPoint = origin; Length = 0.0; IsSwitch = false })
            let tempDrawPieces = pieces |> List.scan calculateNextPoint (seedPiece,seedPiece) |> List.map snd |> Seq.skip 1 |> Seq.toList

            let boundingBox = new Rectangle(int origin.X, int origin.Y, 1,1)
            let box = tempDrawPieces |> List.fold growBoundingBox boundingBox

            background <- new Bitmap(box.Width, box.Height)
            let origin = { X=origin.X - float box.X; Y=origin.Y - float box.Y; A=origin.A }
            
            let seedPiece = DrawStraight ({ StartPoint=origin; EndPoint = origin; Length = 0.0; IsSwitch = false })
            drawPieces <- pieces |> List.scan calculateNextPoint (seedPiece,seedPiece) |> List.map snd |> Seq.skip 1 |> Seq.toList

            lanes <- race.Track.Lanes |> Map.toList |> List.map snd
            let g = Graphics.FromImage(background)
            g.Clear(Color.Black)
            drawPieces |> List.iter (drawPiece g lanes) |> ignore

            background

        background <- generateImage race
        picTrack.Image <- new Bitmap(background)

        txtTrack.Text <- sprintf "%s (%s)" race.Track.Name race.Track.Id
        txtTotalLaps.Text <- match race.Session with
                             | Qualifying x -> ""
                             | QuickRace x -> x.LapCount.ToString()
                             | FullRace x -> x.LapCount.ToString()
            
        txtStage.Text <- match race.Session with
                         | Qualifying x -> "Qualifying"
                         | QuickRace x -> "Quick Race"
                         | FullRace x -> "Full Race"
    
        let generateTargetVelData state = 
            let avg (xs) = xs |> Seq.averageBy (fun (_,v) -> v.Velocity)

            let keys = state.TargetVelKeys
            let kvps = keys |> Seq.map (fun k -> k.PId, state.TargetVels.[k])
            let groups = kvps |> Seq.groupBy (fun (k,v) -> k)
            let averages = groups |> Seq.map (fun (id,xs) -> id, avg xs)
            averages |> Seq.map (fun (id,vel) -> sprintf "%d" id, vel)

        let generateVelocityData state = 
            let data = 
                let ts = state.TelemetryHistory
                match ts.Length with
                | x when x > 1000 -> ts |> Seq.take 1000
                | x -> ts |> List.toSeq
            
            data |> Seq.map (fun t -> t.DistInLap, t.Vel)

        let generateThrottleData state = 
            let data = 
                let ts = state.TelemetryHistory
                match ts.Length with
                | x when x > 100 -> ts |> Seq.take 100
                | x -> ts |> List.toSeq
            
            data |> Seq.mapi (fun i t -> 100 - i, t.Throttle)

        let generateAngleData state = 
            let data = 
                let ts = state.TelemetryHistory
                match ts.Length with
                | x when x > 1000 -> ts |> Seq.take 1000
                | x -> ts |> List.toSeq
                
            data |> Seq.map (fun t -> t.DistInLap, t.Ang * 180.0 / Math.PI)

        chartTimer.Tick.Add (fun args -> targetVelocityChartEvent.Trigger (generateTargetVelData gameStateSample.Value))
        chartTimer.Tick.Add (fun args -> velocityChartEvent.Trigger (generateVelocityData gameStateSample.Value))
        chartTimer.Tick.Add (fun args -> throttleChartEvent.Trigger (generateThrottleData gameStateSample.Value))
        chartTimer.Tick.Add (fun args -> angleChartEvent.Trigger (generateAngleData gameStateSample.Value))

        chartTimer.Tick.Add (fun args -> updateCarPosition (gameStateSample.Value.CarPos))
        chartTimer.Start()        

        if not chartDisplayed then combinedChart.ShowChart()
        chartDisplayed <- true


    member this.updateState state = 
        gameStateSample := state

    member this.updateGameTick gt = 
        txtGameTick.Text <- gt.ToString()
    
    member this.updateInputData (throttle:float) (switch:Direction option) (turboStatus: TurboStatus) =
        txtThrottle.Text <- throttle.ToString("0.00")
       
        txtLane.Text <- match switch with 
                        | Some Left -> "  <-- " + txtLane.Text
                        | Some Right -> txtLane.Text + " -->"
                        | None -> txtLane.Text
        
        txtTurbo.Text <- match turboStatus.ActiveTurbo with 
                         | Some (t, count) -> sprintf "x%s (%d) in use" (t.Factor.ToString("#0.0")) count
                         | None -> 
                            match turboStatus.AvailableTurbo with
                            |Some t -> sprintf "x%s (%d) available" (t.Factor.ToString("#0.0")) t.Ticks
                            |None -> ""

    member this.updateTelemetry (vel:float) (acc:float) (angVel:float) (angAcc:float) =
        txtVel.Text <- vel.ToString("0.00") 
        txtAcc.Text <- acc.ToString("0.00") 
       
        txtAngVel.Text <- angVel.ToString("0.00") 
        txtAngAcc.Text <- angAcc.ToString("0.00") 

    member this.updateRaceLap (lap:int) =
        txtRaceLap.Text <- lap.ToString()

    member this.updateRaceDetailsLap (lap:int) =
        txtRaceLap.Text <- lap.ToString()
         
    member this.AppendLogMessage gt msg =
                
        let msg = sprintf "%d: %s" gt msg

        logStringBuilder.AppendLine(msg) |> ignore

        updateLogUI()
        
    member this.SetCloseAction x =
        closeAction <- x
        