﻿module UiUpdater

open System
open System.IO
open System.Net.Sockets
open System.Windows.Forms

open Messaging
open Visualisation
open MainForm

type UiUpdater (mainForm:MainForm, visForm:VisualisationForm) = 

    let logMessageToUi msg = fun () -> 
        visForm.AppendLogMessage msg

    let raceStartAction state r = fun () -> 
        visForm.setRace r
        visForm.Show()

    let raceEndAction (state:GameStateSpec) = fun () -> 
        
        let printTelem i t =

            let piece = state.Race.Track.Pieces.[t.CarPos.PieceId]
                
            let startRadius,endRadius = 
                match piece.Spec with 
                | Bend b -> 
                    let startOffset = piece.Lanes.[t.CarPos.StartLane].Lane.DistFromCentre
                    let endOffset = piece.Lanes.[t.CarPos.EndLane].Lane.DistFromCentre 

                    if b.Angle < 0.0 then b.Radius + startOffset,b.Radius + endOffset
                    else b.Radius - startOffset,b.Radius - endOffset
                | Straight s -> 0.0, 0.0
            
            
            sprintf "%d,%0.24f,%0.24f,%0.24f,%0.24f,%0.24f,%0.24f,%0.24f,%f,%f" i t.DistInLap t.Vel t.Acc t.Ang t.AngVel t.AngAcc t.Throttle startRadius endRadius

        let telemOutput = 
            [| yield "GameTick,Dist,Vel,Acc,Ang,AngVel,AngAcc,Throttle,StartRadius,EndRadius"
               yield! state.TelemetryHistory |> List.rev |> Seq.mapi printTelem |]
        let logPath = (String.Format("c:\\temp\Telem-{0:yyMMdd-HHmmss} Telem.csv", DateTime.Now))             
        File.WriteAllLines(logPath, telemOutput)
        mainForm.StartRaceEnabled <- true

    let updateGameTick gameState = fun () -> 
        visForm.updateGameTick (gameState.GameTick)

    let updateCarTelemetry carId state = fun () -> 

        let switch = 
            match state.SwitchRequested with
            | Some d when d < state.CarPos.EndLane -> Some Left
            | Some d when d > state.CarPos.EndLane -> Some Right
            | _ -> None
        
        let throttle = state.Telemetry.Throttle
        
        visForm.updateInputData throttle switch state.TurboStatus

        let tel = state.Telemetry

        let toDegrees radians = radians * 180. / System.Math.PI
                            
        visForm.updateTelemetry tel.Vel tel.Acc (toDegrees tel.AngVel) (toDegrees tel.AngAcc)

        visForm.updateState state

    let invokeOnUiThread action = 
        if not mainForm.IsDisposed then
            mainForm.Invoke(new MethodInvoker(action)) |> ignore

    let agent = MailboxProcessor.Start(fun inbox -> 

        let rec messageLoop () = 
            async {

                let! state = inbox.Receive()

                match state.MessageReceived with
                | Race r -> 
                    r |> (raceStartAction state) |> invokeOnUiThread
                | GameEnd _ -> (raceEndAction state) |> invokeOnUiThread
                | _ -> ()

                if state.GameTick > 0 then
                    updateGameTick state |> invokeOnUiThread |> ignore
                    (updateCarTelemetry state.YourCar state) |> invokeOnUiThread

                return! messageLoop ()
            }

        messageLoop ()
    )

    member this.update msg = agent.Post msg

