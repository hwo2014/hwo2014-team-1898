﻿module MainForm

open System
open System.Drawing
open System.Threading
open System.Windows.Forms

open GameLoop

type MainForm (host, port, botName, botKey, log) as this = 
    inherit Form()

    let mutable updateAction = fun _ -> ()
    let mutable cts = null

    let lblServer = new Label()
    let grpGameProperties = new GroupBox()
    let lblBotName = new Label()
    let lblBotKey = new Label()
    let txtServer = new TextBox()
    let txtPort = new TextBox()
    let txtBotName = new TextBox()
    let txtBotKey = new TextBox()
    let lblTrack = new Label()
    let chkAdvancedRace = new CheckBox()
    let cmbTrack = new ComboBox()
    let lblCarCount = new Label()
    let txtCarCount = new TextBox()
    let btnStartRace = new Button()
    let btnLeaveRace = new Button()
    
    do
        grpGameProperties.SuspendLayout()
        this.SuspendLayout()
        // 
        // lblServer
        // 
        lblServer.AutoSize <- true
        lblServer.Font <- new Font("Calibri", 10.2F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)))
        lblServer.Location <- new Point(8, 25)
        lblServer.Name <- "lblServer"
        lblServer.Size <- new Size(54, 21)
        lblServer.TabIndex <- 0
        lblServer.Text <- "Server"
        // 
        // grpGameProperties
        // 
        grpGameProperties.Controls.Add(txtCarCount)
        grpGameProperties.Controls.Add(lblCarCount)
        grpGameProperties.Controls.Add(cmbTrack)
        grpGameProperties.Controls.Add(chkAdvancedRace)
        grpGameProperties.Controls.Add(lblTrack)
        grpGameProperties.Controls.Add(txtBotKey)
        grpGameProperties.Controls.Add(txtBotName)
        grpGameProperties.Controls.Add(txtPort)
        grpGameProperties.Controls.Add(txtServer)
        grpGameProperties.Controls.Add(lblBotKey)
        grpGameProperties.Controls.Add(lblBotName)
        grpGameProperties.Controls.Add(lblServer)
        grpGameProperties.Location <- new Point(12, 12)
        grpGameProperties.Name <- "grpGameProperties"
        grpGameProperties.Size <- new Size(547, 161)
        grpGameProperties.TabIndex <- 2
        grpGameProperties.TabStop <- false
        grpGameProperties.Text <- "Game Properties"
        // 
        // lblBotName
        // 
        lblBotName.AutoSize <- true
        lblBotName.Font <- new Font("Calibri", 10.2F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)))
        lblBotName.Location <- new Point(8, 53)
        lblBotName.Name <- "lblBotName"
        lblBotName.Size <- new Size(77, 21)
        lblBotName.TabIndex <- 2
        lblBotName.Text <- "Bot name"
        // 
        // lblBotKey
        // 
        lblBotKey.AutoSize <- true
        lblBotKey.Font <- new Font("Calibri", 10.2F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)))
        lblBotKey.Location <- new Point(296, 55)
        lblBotKey.Name <- "lblBotKey"
        lblBotKey.Size <- new Size(61, 21)
        lblBotKey.TabIndex <- 3
        lblBotKey.Text <- "Bot key"
        // 
        // txtServer
        // 
        txtServer.Location <- new Point(93, 25)
        txtServer.Name <- "txtServer"
        txtServer.Size <- new Size(353, 22)
        txtServer.TabIndex <- 4
        txtServer.Text <- host
        // 
        // txtPort
        // 
        txtPort.Location <- new Point(452, 25)
        txtPort.Name <- "txtPort"
        txtPort.Size <- new Size(80, 22)
        txtPort.TabIndex <- 5
        txtPort.Text <- port
        // 
        // txtBotName
        // 
        txtBotName.Location <- new Point(93, 55)
        txtBotName.Name <- "txtBotName"
        txtBotName.Size <- new Size(183, 22)
        txtBotName.TabIndex <- 6
        txtBotName.Text <- botName
        // 
        // txtBotKey
        // 
        txtBotKey.Location <- new Point(363, 54)
        txtBotKey.Name <- "txtBotKey"
        txtBotKey.Size <- new Size(169, 22)
        txtBotKey.TabIndex <- 7
        txtBotKey.Text <- botKey
        // 
        // lblTrack
        // 
        lblTrack.AutoSize <- true
        lblTrack.Font <- new Font("Calibri", 10.2F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)))
        lblTrack.Location <- new Point(8, 121)
        lblTrack.Name <- "lblTrack"
        lblTrack.Size <- new Size(46, 21)
        lblTrack.TabIndex <- 8
        lblTrack.Text <- "Track"
        // 
        // chkAdvancedRace
        // 
        chkAdvancedRace.AutoSize <- true
        chkAdvancedRace.Location <- new Point(12, 95)
        chkAdvancedRace.Name <- "chkAdvancedRace"
        chkAdvancedRace.Size <- new Size(196, 21)
        chkAdvancedRace.TabIndex <- 9
        chkAdvancedRace.Text <- "Specify Advanced Options"
        chkAdvancedRace.UseVisualStyleBackColor <- true
        chkAdvancedRace.Checked <- true
        // 
        // cmbTrack
        // 
        cmbTrack.FormattingEnabled <- true
        cmbTrack.Items.AddRange([| "keimola";"germany";"usa";"france";"elaeintarha";"imola";"england";"suzuka";"pentag" |])
        cmbTrack.Location <- new Point(93, 121)
        cmbTrack.Name <- "cmbTrack"
        cmbTrack.Size <- new Size(183, 24)
        cmbTrack.TabIndex <- 10
        cmbTrack.SelectedIndex <- 0
        // 
        // lblCarCount
        // 
        lblCarCount.AutoSize <- true
        lblCarCount.Font <- new Font("Calibri", 10.2f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)))
        lblCarCount.Location <- new Point(296, 124)
        lblCarCount.Name <- "lblCarCount"
        lblCarCount.Size <- new Size(79, 21)
        lblCarCount.TabIndex <- 11
        lblCarCount.Text <- "Car Count"
        // 
        // txtCarCount
        // 
        txtCarCount.Location <- new Point(381, 124)
        txtCarCount.Name <- "txtCarCount"
        txtCarCount.Size <- new Size(80, 22)
        txtCarCount.TabIndex <- 12
        txtCarCount.Text <- "1"
        // 
        // btnStartRace
        // 
        btnStartRace.Font <- new Font("Calibri", 19.8F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)))
        btnStartRace.Location <- new Point(12, 180)
        btnStartRace.Name <- "btnStartRace"
        btnStartRace.Size <- new Size(547, 49)
        btnStartRace.TabIndex <- 3
        btnStartRace.Text <- "Start Racing!!!"
        btnStartRace.UseVisualStyleBackColor <- true
        btnStartRace.Click.Add(fun e -> 
        
            let host = txtServer.Text
            let port = Int32.Parse(txtPort.Text)
            let botName = txtBotName.Text
            let botKey = txtBotKey.Text

            let joinMsg = 
                match chkAdvancedRace.Checked with 
                | true -> 
                    let carCount = Int32.Parse(txtCarCount.Text)
                    let track = cmbTrack.SelectedItem :?> string
                    AdvancedJoin (botName,botKey,track,carCount)
                | false -> SimpleJoin (botName, botKey)

            this.StartRaceEnabled <- false

            let game = new Game(host, port, updateAction, log, if true then basicAI else DaveAI.aiHandler)
            
            cts <- new CancellationTokenSource()
            game.start joinMsg false cts
        ) 
        // 
        // btnCancelRace
        // 
        btnLeaveRace.Font <- new Font("Calibri", 19.8F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)))
        btnLeaveRace.Location <- new Point(12, 180)
        btnLeaveRace.Name <- "btnCancelRace"
        btnLeaveRace.Size <- new Size(547, 49)
        btnLeaveRace.TabIndex <- 3
        btnLeaveRace.Text <- "Leave Race"
        btnLeaveRace.UseVisualStyleBackColor <- true
        btnLeaveRace.Enabled <- false
        btnLeaveRace.Visible <- false
        btnLeaveRace.Click.Add(fun e ->
            cts.Cancel()
            this.StartRaceEnabled <- true 
        ) 
        //
        // Main form
        //
        this.AutoScaleDimensions <- new SizeF(8.f, 16.f)
        this.AutoScaleMode <- AutoScaleMode.Font
        this.ClientSize <- new Size(569, 240)
        this.Controls.Add(btnStartRace)
        this.Controls.Add(btnLeaveRace)
        this.Controls.Add(grpGameProperties)
        this.Name <- "Car Driver"
        this.Text <- "Car Driver"
        grpGameProperties.ResumeLayout(false)
        grpGameProperties.PerformLayout()
        this.ResumeLayout(false)

    member this.UpdateAction
        with get() = updateAction
        and set(value) = updateAction <- value

    member this.StartRaceEnabled
        with get() = btnStartRace.Enabled
        and set(value) = 
            btnLeaveRace.Visible <- not value
            btnStartRace.Visible <- value   
            btnStartRace.Enabled <- value
            btnLeaveRace.Enabled <- not value
    
    member this.CancelRace() = 
        cts.Cancel()
        this.StartRaceEnabled <- true 